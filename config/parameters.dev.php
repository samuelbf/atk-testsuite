<?php

return [
    'atk' => [

        'identifier' => 'atk-testsuite',

        'db' => [
            'default' => [
                'host' =>  getenv('DB_HOST'),
                'db' => getenv('DB_DATABASE'),
                'user' => getenv('DB_USER'),
                'password' => getenv('DB_PASSWORD'),
                'charset' => getenv('DB_DRIVER') == 'mysql' ? 'utf8mb4' : 'utf8',
                'driver' => getenv('DB_DRIVER') == 'mysql' ? 'MySqli' :
                    (getenv('DB_DRIVER') == 'pgsql' ? 'PgSql' : 'sqlite'),
                'file' => '../'.getenv('DB_FILE'),
            ],
        ],

        'debug' => 1,
        'meta_caching' => false,
        'auth_ignorepasswordmatch' => false,
        'administratorpassword' => '$2y$10$erDvMUhORJraJyxw9KXKKOn7D1FZNsaiT.g2Rdl/4V6qbkulOjUqi', // administrator
    ],
];
