<?php
namespace App\Modules\App;

use Sintattica\Atk\Attributes\Attribute;
use Sintattica\Atk\Attributes\DummyAttribute;
use Sintattica\Atk\Core\Node;
use Sintattica\Atk\Relations\ManyToManySelectRelation;

/**
 * Furniture node is used to illustrate ManyToMany relations
 */
class Furniture extends Node
{
    public function __construct($nodeUri)
    {
        parent::__construct($nodeUri, Node::NF_ADD_LINK | Node::NF_MRA | Node::NF_EXPORT);
        $this->setTable('app_furniture');
        $this->setDescriptorTemplate('[name]');
        $this->setOrder('[table].name');
        
        $this->add(new Attribute('id', Attribute::AF_AUTOKEY));
        $this->add(new Attribute('name', Attribute::AF_UNIQUE|Attribute::AF_SEARCHABLE));
        
        $this->add(new ManyToManySelectRelation(
            'holders',
            ManyToManySelectRelation::AF_MANYTOMANY_DETAILVIEW|Attribute::AF_SEARCHABLE,
            'App.employeeFurniture',
            'App.employeeRelation',
            'furniture',
            'employee'
        ));

        $this->add(new DummyAttribute(
            'Warning',
            Attribute::AF_HIDE_LIST,
            'This guy... donald ... is known for breaking things you give to him'
        ));

        $this->getAttribute('holders')->addDependency(function ($editform) {
            $holderRelation = $editform->getNode()->getAttribute('holders');
            $showWarning = false;
            foreach ($editform->getRecord()['holders'] as $holder) {
                $holder = $holder['employee'];
                if (!is_array($holder)) {
                    $holder = $holderRelation->getDestination()->fetchByPk($holder);
                }
                if (substr($holder['name'], 0, 6) == 'Donald') {
                    $showWarning = true;
                }
            }
            if ($showWarning) {
                $editform->showAttribute('Warning');
            } else {
                $editform->hideAttribute('Warning');
            }
        });
    }
}
