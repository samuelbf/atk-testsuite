<?php

namespace App\Modules\App;

use Sintattica\Atk\Attributes\Attribute;
use Sintattica\Atk\Core\Node;
use Sintattica\Atk\Relations\ManyToOneRelation;

/**
 * Glue node for employee-furniture link.
 */
class EmployeeFurniture extends Node
{
    public function __construct($nodeUri)
    {
        parent::__construct($nodeUri);
        $this->setTable('app_employee_furniture');

        $this->add(new ManyToOneRelation('employee', Attribute::AF_PRIMARY, 'App.employeeRelation'));
        $this->add(new ManyToOneRelation('furniture', Attribute::AF_PRIMARY, 'App.furniture'));
    }
}
