<?php
namespace App\Modules\App;

use Sintattica\Atk\Attributes\Attribute;
use Sintattica\Atk\Attributes\DateAttribute;
use Sintattica\Atk\Attributes\NumberAttribute;
use Sintattica\Atk\Relations\ManyToOneRelation;
use Sintattica\Atk\Relations\OneToManyRelation;
use Sintattica\Atk\Core\Node;

/**
 * Book node is used to illustrate UTF-8 in table characters
 * To read/edit this file, you will need a font that covers large parts of utf-8 encoding,
 * such as symbola : http://users.teilar.gr/~g1951d/
 */
class Book extends Node
{
    public function __construct($nodeUri)
    {
        parent::__construct($nodeUri, Node::NF_ADD_LINK | Node::NF_MRA | Node::NF_EXPORT);
        $this->setTable('app_📚');
        $this->setDescriptorTemplate('["title" ?]');
        $this->setOrder('[table]."""title"" ?"');

        $this->add(new Attribute('Numéro d\'identification', Attribute::AF_AUTOKEY));
        $this->add(new Attribute('"title" ?', Attribute::AF_UNIQUE|Attribute::AF_SEARCHABLE))
            ->addDependency(function ($editform) {
                if ($editform->getRecord()['"title" ?'] == '') {
                    $editform->hideAttribute('ex ़ॏॠ༛ᇻቡᙻ⏏⏰⑰漣年');
                } else {
                    $editform->showAttribute('ex ़ॏॠ༛ᇻቡᙻ⏏⏰⑰漣年');
                }
            });
        $this->add(new Attribute('👤', Attribute::AF_SEARCHABLE));
        $this->add(new DateAttribute('🗓', Attribute::AF_SEARCHABLE));
        $this->add(new NumberAttribute('"..., FROM "` CREATE TABLE ;)  -? pÜre N😐nsense', Attribute::AF_SEARCHABLE));
        $this->add(new NumberAttribute('ex ़ॏॠ༛ᇻቡᙻ⏏⏰⑰漣年', Attribute::AF_SEARCHABLE));
        $this->add(new ManyToOneRelation(
            '↺',
            Attribute::AF_SEARCHABLE|ManyToOneRelation::AF_RELATION_AUTOLINK,
            'App.book'
        ));
        $this->add(new OneToManyRelation(
            'Child books ⇘',
            Attribute::AF_SEARCHABLE|Attribute::AF_HIDE_LIST,
            'App.book',
            '↺'
        ));
    }
}
