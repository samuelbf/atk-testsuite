<?php
namespace App\Modules\App;

use Sintattica\Atk\Attributes\Attribute;
use Sintattica\Atk\Attributes\TextAttribute;
use Sintattica\Atk\Core\Node;

/**
 * Address node is a target for OneToOne relation from Employee
 */
class Address extends Node
{
    public function __construct($nodeUri)
    {
        parent::__construct($nodeUri);
        $this->setTable('app_address');
        $this->setDescriptorTemplate('[city]');
        
        $this->add(new Attribute('id', Attribute::AF_AUTOKEY));
        $this->add(new TextAttribute('address', 0, ['rows' => 2]));
        $this->add(new Attribute('zipcode', Attribute::AF_OBLIGATORY));
        $this->add(new Attribute('city', Attribute::AF_OBLIGATORY));
    }
}
