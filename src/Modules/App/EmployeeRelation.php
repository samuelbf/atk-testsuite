<?php
namespace App\Modules\App;

use Sintattica\Atk\Core\Node;
use Sintattica\Atk\Attributes\Attribute;
use Sintattica\Atk\Relations\ShuttleRelation;
use Sintattica\Atk\Relations\ManyToOneRelation;
use Sintattica\Atk\Relations\OneToOneRelation;

/**
 * EmployeeRelation is the employee node, but only for relations tests.
 */
class EmployeeRelation extends Node
{
    public function __construct($nodeUri)
    {
        parent::__construct($nodeUri, Node::NF_ADD_LINK | Node::NF_TRACK_CHANGES | Node::NF_MRA | Node::NF_EXPORT);
        $this->setTable('app_employee');
        $this->setDescriptorTemplate('[name] ([email])');
        $this->setOrder('[table].name');

        $this->add(new Attribute('id', Attribute::AF_AUTOKEY));
        $this->add(new Attribute('name', Attribute::AF_SEARCHABLE|Attribute::AF_UNIQUE|Attribute::AF_OBLIGATORY));
        $this->add(new Attribute('email', Attribute::AF_SEARCHABLE));

        $dpt = $this->add(new ManyToOneRelation(
            'department',
            Attribute::AF_SEARCHABLE | ManyToOneRelation::AF_RELATION_AUTOLINK,
            'App.department'
        ));
        $dpt->addListColumn('statuses');

        // Dummy destination filter
        $dpt->addDestinationFilter(
            "app_department.statuses IS NULL OR app_department.statuses NOT LIKE '%[email]_soon%'"
        );
        $dpt->setMultipleSearch(true, true);

        $this->add(new OneToOneRelation('address', Attribute::AF_SEARCHABLE, 'App.address'));
        $this->add(new ManyToOneRelation(
            'manager',
            Attribute::AF_SEARCHABLE|Attribute::AF_LARGE,
            'App.employeeRelation'
        ));

        $this->add(new ShuttleRelation(
            'furniture',
            ShuttleRelation::AF_MANYTOMANY_DETAILVIEW|Attribute::AF_SEARCHABLE,
            'App.employeeFurniture',
            'App.furniture',
            'employee',
            'furniture'
        ));
    }

    // Descriptor function used in Department class
    public function emails_descriptor($record = [])
    {
        return $record['email'] ?? 'No mail set !';
    }
}
