<?php
namespace App\Modules\App;

/**
 * The module definition class.
 */
class Module extends \Sintattica\Atk\Core\Module
{
    public static $module = 'App';

    public function register()
    {
        $this->registerNode('department', Department::class, ['admin', 'add', 'edit', 'delete']);
        $this->registerNode('employee', Employee::class, ['admin', 'add', 'edit', 'delete', 'raise_salary']);
        $this->registerNode('employeeRelation', EmployeeRelation::class, ['admin', 'add', 'edit', 'delete']);
        $this->registerNode('employeeSections', EmployeeSections::class, ['admin', 'add', 'edit', 'delete']);
        $this->registerNode('employeeTree', EmployeeTree::class, ['admin', 'add', 'edit', 'delete']);
        $this->registerNode('employeeFilter', EmployeeFilter::class, ['admin', 'add', 'edit', 'delete']);
        $this->registerNode('furniture', Furniture::class, ['admin', 'add', 'edit', 'delete']);
        $this->registerNode('employeeFurniture', EmployeeFurniture::class);
        $this->registerNode('book', Book::class, ['admin','add', 'edit', 'delete']);
        $this->registerNode('address', Address::class, ['add', 'edit', 'delete']);
        $this->registerNode('addressSections', AddressSections::class, ['add', 'edit', 'delete']);
        $this->registerNode('project', Project::class, ['admin', 'add', 'edit', 'delete']);
        $this->registerNode('projectEmployee', ProjectEmployee::class);
        $this->registerNode('log', Log::class, ['admin']);
    }

    public function boot()
    {
        $this->addNodeToMenu('departments', 'department', 'admin');
        $this->addNodeToMenu('projects', 'project', 'admin');
        $this->addNodeToMenu('furnitures', 'furniture', 'admin');
        $this->addNodeToMenu('books', 'book', 'admin');
        $this->addMenuItem('employees');
        $this->addNodeToMenu('attributes', 'employee', 'admin', 'employees');
        $this->addNodeToMenu('relations', 'employeeRelation', 'admin', 'employees');
        $this->addNodeToMenu('Sections & Tabs', 'employeeSections', 'admin', 'employees');
        $this->addNodeToMenu('Organigram', 'employeeTree', 'admin', 'employees');
        $this->addNodeToMenu('Filters', 'employeeFilter', 'admin', 'employees');
        $this->addNodeToMenu('Logs', 'log', 'admin');
    }
}
