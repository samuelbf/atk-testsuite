<?php
namespace App\Modules\App;

use Sintattica\Atk\Core\Node;
use Sintattica\Atk\Attributes\AggregatedColumn;
use Sintattica\Atk\Attributes\Attribute;
use Sintattica\Atk\Attributes\BoolAttribute;
use Sintattica\Atk\Attributes\ColorPickerAttribute;
use Sintattica\Atk\Attributes\CreatedByAttribute;
use Sintattica\Atk\Attributes\CreateStampAttribute;
use Sintattica\Atk\Attributes\CurrencyAttribute;
use Sintattica\Atk\Attributes\DateTimeAttribute;
use Sintattica\Atk\Attributes\ExpressionAttribute;
use Sintattica\Atk\Attributes\DummyAttribute;
use Sintattica\Atk\Attributes\EmailAttribute;
use Sintattica\Atk\Attributes\FlagAttribute;
use Sintattica\Atk\Attributes\ListAttribute;
use Sintattica\Atk\Attributes\NumberAttribute;
use Sintattica\Atk\Attributes\TextAttribute;
use Sintattica\Atk\Attributes\UpdatedByAttribute;
use Sintattica\Atk\Attributes\UpdateStampAttribute;
use Sintattica\Atk\Attributes\UrlAttribute;
use Sintattica\Atk\Utils\EventLog;
use Sintattica\Atk\Core\Tools;

/**
 * Employee is the main node, with lots of attributes
 */
class Employee extends Node
{
    public function __construct($nodeUri)
    {
        parent::__construct($nodeUri, Node::NF_ADD_LINK | Node::NF_TRACK_CHANGES | Node::NF_EXPORT | Node::NF_MRA);
        $this->setTable('app_employee');
        $this->setDescriptorTemplate('[name]');
        $this->setOrder('[table].name');

        $this->add(new Attribute('id', Attribute::AF_AUTOKEY));
        $this->add(new ListAttribute('gender', 0, ['M', 'F', 'O']))->setInitialValue('F');
        $this->add(new Attribute('name', Attribute::AF_SEARCHABLE|Attribute::AF_UNIQUE|Attribute::AF_OBLIGATORY));
        $this->add(new EmailAttribute('email'));
        $this->add(new NumberAttribute('age', Attribute::AF_HIDE_LIST))->setInitialValue(15);
        $this->getAttribute('age')->setRange(0, 99);
        $this->getAttribute('age')->addDependency(
            function ($editform) {
                $record = $editform->getRecord();
                if (!empty($record['age']) && empty($record['color'])) {
                    $editform->refreshAttribute('color');
                }
            }
        );

        $this->add(new ColorPickerAttribute('color', Attribute::AF_HIDE_LIST, ['format' => 'hex']));
        $this->add(new DummyAttribute(
            'gdpr',
            DummyAttribute::AF_DUMMY_SHOW_LABEL | Attribute::AF_HIDE_LIST,
            'Storing the color preferences of employees may not be GDPR-compliant... fill only when necessary.'
        ));
        $this->add(new BoolAttribute('vacation', Attribute::AF_SEARCHABLE))->setInitialValue(1);
        $this->add(new DateTimeAttribute('hiredate', Attribute::AF_SEARCHABLE));
        $this->add(new TextAttribute('notes', Attribute::AF_HIDE_LIST));
        $this->add(new UrlAttribute('blog', Attribute::AF_HIDE_LIST));
        $this->add(new AggregatedColumn('characteristics', Attribute::AF_SEARCHABLE, '[age] - [color] - [notes]'));
        $this->add(new FlagAttribute(
            'qualities',
            Attribute::AF_HIDE_LIST,
            ['nice', 'punctual', 'serious', 'generous', 'inclusive', 'encouraging', 'dynamic'],
            [1<<0, 1<<1, 1<<2, 1<<3, 1<<4, 1<<5, 1<<6]
        ))->setInitialValue(1<<0 | 1<<5);
        $this->add(new CurrencyAttribute('salary', Attribute::AF_TOTAL, '€'))->setInitialValue(2000);
        switch (getenv('DB_DRIVER')) {
            case 'mysql':
                $expression = 'DATEDIFF(NOW(), hiredate)*salary/30';
                break;
            case 'pgsql':
                $expression = 'extract(day from NOW()-date(hiredate))*salary/30';
                break;
            case 'sqlite':
                $expression = "(cast((julianday('now')-0.5) as int)-cast((julianday(hiredate)-0.5) as int))*salary/30";
                break;
        }
        $this->add(new ExpressionAttribute('total_salary', Attribute::AF_HIDE_LIST, $expression));
        $this->add(new CreatedByAttribute('createdby', Attribute::AF_HIDE_LIST));
        $this->add(new CreateStampAttribute('createstamp', Attribute::AF_HIDE_LIST));
        $this->add(new UpdatedByAttribute('updatedby', Attribute::AF_HIDE_LIST));
        $this->add(new UpdateStampAttribute('updatestamp', Attribute::AF_HIDE_LIST));

        $this->addListener(new EventLog());
    }

    public function rowColor($record)
    {
        $manager_id = $record['manager']['id'];
        if ($manager_id!="") {
            $salary = $record['salary'];

            $managerNode = $this->getAttribute('manager')->getDestination();
            $managerRecord = $managerNode->select("id=$manager_id")->includes('salary')->getFirstRow();
            $managerSalary = $managerRecord['salary'];

            if ($salary > $managerSalary) {
                return '#ff0000';
            }
        }
    }

    public function recordActions($record, &$actions, &$mraactions)
    {
        $mraactions[] = 'raise_salary';
        $actions['raise_salary'] = Tools::dispatch_url(
            $this->atkNodeUri,
            'raise_salary',
            ['atkselector' => '[pk]']
        );
        parent::recordActions($record, $actions, $mraactions);
    }

    public function postAdd($record, $mode = 'add')
    {
        // TODO : write something to test this function.
    }

    public function postUpdate($record)
    {
        // TODO : write something to test this function.
    }

    public function postDelete($record)
    {
        // TODO : write something to test this function.
    }

    // Some weird function that derives a color from the 'age' variable.
    public function color_edit($record, $fieldprefix, $mode)
    {
        $color = $this->getAttribute('color');
        $age = $record['age'];
        if (!empty($age) && empty($record['color'])) {
            $record['color'] = '#'.sprintf('%02x%02x%02x', $age*2.56, 256-$age*2.56, (128+$age*2.56) % 256);
        }
        return $color->edit($record, $fieldprefix, $mode);
    }

    // Let's add 150€/month for selected employees
    public function action_raise_salary()
    {
        $employees = $this->select($this->m_postvars['atkselector'])->getAllRows();
        foreach ($employees as $employee) {
            $employee['salary'] += 150;
            $this->updateDb($employee, true, '', 'salary');
        }
        $this->getDb()->commit();
        $this->redirect();
    }
}
