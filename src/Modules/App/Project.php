<?php
namespace App\Modules\App;

use Sintattica\Atk\Attributes\Attribute;
use Sintattica\Atk\Attributes\DateAttribute;
use Sintattica\Atk\Attributes\TextAttribute;
use Sintattica\Atk\Core\Node;
use Sintattica\Atk\Relations\ManyBoolRelation;

/**
 * Project node is used to illustrate ManyToMany relations and complex primary keys
 */
class Project extends Node
{
    public function __construct($nodeUri)
    {
        parent::__construct($nodeUri, Node::NF_ADD_LINK | Node::NF_MRA | Node::NF_EXPORT);
        $this->setTable('app_project');
        $this->setDescriptorTemplate('[name]');
        $this->setOrder('[table].name');
        
        $this->add(new Attribute('name', Attribute::AF_PRIMARY|Attribute::AF_UNIQUE));
        $this->add(new DateAttribute('startdate', Attribute::AF_PRIMARY|Attribute::AF_UNIQUE));
        $this->add(new TextAttribute('description'));
        
        $this->add(new ManyBoolRelation(
            'members',
            ManyBoolRelation::AF_MANYTOMANY_DETAILVIEW,
            'App.projectEmployee',
            'App.employee',
            ['name', 'startdate'],
            'employee'
        ));
    }
}
