<?php
namespace App\Modules\App;

use Sintattica\Atk\Attributes\Attribute;
use Sintattica\Atk\Attributes\TextAttribute;
use Sintattica\Atk\Core\Node;

/**
 * AddressSections node is a target for OneToOne relation from EmployeeSections
 */
class AddressSections extends Address
{
    public function __construct($nodeUri)
    {
        parent::__construct($nodeUri);

        $this->getAttribute('zipcode')->setTabs(['city']);
        $this->getAttribute('city')->setTabs(['city']);
    }
}
