<?php
namespace App\Modules\App;

use Sintattica\Atk\Attributes\Attribute;
use Sintattica\Atk\Attributes\DummyAttribute;
use Sintattica\Atk\Attributes\TabbedPane;
use Sintattica\Atk\Relations\OneToOneRelation;

/**
 * EmployeeSections illustrates sections, tabs and tabbedpanes
 */
class EmployeeSections extends Employee
{
    public function __construct($nodeUri)
    {
        $this->add(new DummyAttribute('Persinf', 0, 'Personal informations'), '.Personal information');
        $this->add(new DummyAttribute('hr_label', 0, 'Human resource informations'), '.HR');
        $this->add(new DummyAttribute(
            'Part-time_Attribute',
            Attribute::AF_HIDE ^ Attribute::AF_HIDE_ADD,
            'This attribute only shows up in add form'
        ), 'Part-time_tab');

        parent::__construct($nodeUri);
        $this->getAttribute('name')->setSections(['.Personal information']);
        $this->getAttribute('gender')->setSections(['.Personal information']);
        $this->getAttribute('email')->setSections(['.Personal information']);
        $this->getAttribute('salary')->setSections(['.HR']);
        $this->getAttribute('hiredate')->setSections(['.HR']);
        $this->getAttribute('vacation')->setSections(['.HR']);
        $this->getAttribute('notes')->setSections(['.HR']);
        $this->add(new TabbedPane(
            'meta',
            0,
            [
                'creation information' => ['createdby', 'createstamp'],
                'update information' => ['updatedby', 'updatestamp']
            ]
        ), ['some_details']);
        $this->add(new OneToOneRelation('address', OneToOneRelation::AF_ONETOONE_INTEGRATE, 'App.addressSections'));
        $this->getAttribute('color')->setSections(['some_details']);
        $this->getAttribute('gdpr')->setSections(['some_details']);
        $this->getAttribute('blog')->setSections(['*']);
        $this->setAttributeOrder('blog', 50);
        $this->getAttribute('age')->setSections(['', 'first_tab']);

        $this->addDefaultExpandedSections('.Personal information');
        $this->setTabIndex('first_tab', 1);
        $this->setTabIndex('some_details', 2);
    }
}
