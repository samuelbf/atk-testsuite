<?php

namespace App\Modules\App;

use Sintattica\Atk\Attributes\Attribute;
use Sintattica\Atk\Core\Node;
use Sintattica\Atk\Relations\ManyToOneRelation;

/**
 * Glue node for project-employee link.
 */
class ProjectEmployee extends Node
{
    public function __construct($nodeUri)
    {
        parent::__construct($nodeUri);
        $this->setTable('app_project_employee');

        $this->add(new ManyToOneRelation(['name', 'startdate'], Attribute::AF_PRIMARY, 'App.project'));
        $this->add(new ManyToOneRelation('employee', Attribute::AF_PRIMARY, 'App.employee'));
    }
}
