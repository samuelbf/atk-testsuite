<?php
namespace App\Modules\App;

use Sintattica\Atk\Attributes\Attribute;
use Sintattica\Atk\Attributes\MultiSelectListAttribute;
use Sintattica\Atk\Core\Node;
use Sintattica\Atk\Relations\OneToManyRelation;
use Sintattica\Atk\Relations\ManyToOneRelation;

/**
 * Department node is used to illustrate OneToMany relations and relations descriptors
 */
class Department extends Node
{
    public function __construct($nodeUri)
    {
        parent::__construct($nodeUri, Node::NF_ADD_LINK | Node::NF_EXPORT);
        $this->setTable('app_department');
        $this->setDescriptorTemplate('[name]');

        $this->add(new Attribute('id', Attribute::AF_AUTOKEY));
        $this->add(new Attribute('name', Attribute::AF_SEARCHABLE|Attribute::AF_UNIQUE));
        $this->add(new MultiSelectListAttribute(
            'statuses',
            Attribute::AF_SEARCHABLE,
            ['is_hiring', 'will_be_closed_soon', 'need_to_change_boss']
        ))->setMultipleSearch(true, true);

        $this->add(new ManyToOneRelation('manager', Attribute::AF_LARGE | Attribute::AF_SEARCHABLE, 'App.employee'));

        // Members of the department listed with their standard descriptor
        $this->add(new OneToManyRelation('employees', Attribute::AF_SEARCHABLE, 'App.employeeRelation', 'department'));

        // Now listed with a descriptor as a template string (with relations values in it) :
        $this->add(new OneToManyRelation('addresses', Attribute::AF_SEARCHABLE, 'App.employeeRelation', 'department'));
        $this->getAttribute('addresses')->setDescriptorTemplate('[address.zipcode] [address.city]');

        // Now listed with a descriptor as a function inside destination node :
        $this->add(new OneToManyRelation('emails', Attribute::AF_SEARCHABLE, 'App.employeeRelation', 'department'));
        $this->getAttribute('emails')->setDescriptorHandler(new EmployeeRelation(''));
        // it will call emails_descriptor function on dest node.
    }
}
