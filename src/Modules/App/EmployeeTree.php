<?php
namespace App\Modules\App;

use Sintattica\Atk\Attributes\Attribute;
use Sintattica\Atk\Core\TreeNode;
use Sintattica\Atk\Relations\ManyToOneTreeRelation;

/**
 * EmployeeTree present the organigram and allows us to test TreeNodes
 */
class EmployeeTree extends TreeNode
{
    public function __construct($nodeUri)
    {
        parent::__construct($nodeUri, TreeNode::NF_ADD_LINK);
        $this->setTable('app_employee');
        $this->setDescriptorTemplate('[name]');
        $this->setOrder('[table].name');

        $this->add(new Attribute('id', Attribute::AF_AUTOKEY));
        $this->add(new Attribute('name'));
        $this->add(new ManyToOneTreeRelation('manager', Attribute::AF_PARENT, 'App.employeeTree'));
    }
}
