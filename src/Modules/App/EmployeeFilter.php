<?php
namespace App\Modules\App;

/**
 * EmployeeFilter is the like the Employee node, but with filters.
 */
class EmployeeFilter extends Employee
{
    public function __construct($nodeUri)
    {
        parent::__construct($nodeUri);

        $this->addFilter('[table].age > 45');
        $this->addFilter('vacation', true);
    }
}
