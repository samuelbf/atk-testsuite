<?php
namespace App\Modules\App;

use Sintattica\Atk\Core\Node;
use Sintattica\Atk\Attributes\Attribute;
use Sintattica\Atk\Attributes\CreatedByAttribute;
use Sintattica\Atk\Attributes\DateTimeAttribute;

/**
 * Log is used to consult and test the ATK event log
 */
class Log extends Node
{
    public function __construct($nodeUri)
    {
        parent::__construct($nodeUri, Node::NF_EXPORT);
        $this->setTable('atkeventlog');
        $this->setOrder('[table].id DESC');

        $this->add(new Attribute('id', Attribute::AF_AUTOKEY));
        $this->add(new DateTimeAttribute('stamp', Attribute::AF_SEARCHABLE));
        $this->add(new CreatedByAttribute('userid', Attribute::AF_SEARCHABLE));
        $this->add(new Attribute('node', Attribute::AF_SEARCHABLE));
        $this->add(new Attribute('action', Attribute::AF_SEARCHABLE));
        $this->add(new Attribute('primarykey', Attribute::AF_SEARCHABLE));
    }

    public function node_display($record, $mode)
    {
        $url = 'index.php?atknodeuri='.rawurlencode($record['node']).'&atkaction=admin';
        return "<a href='$url'>".$this->getAttribute('node')->display($record, $mode).'</a>';
    }

    public function primarykey_display($record, $mode)
    {
        $url = 'index.php?atknodeuri='.rawurlencode($record['node']).
            '&atkaction=view&atkselector='.rawurlencode($record['primarykey']);
        return "<a href='$url'>".$this->getAttribute('primarykey')->display($record, $mode).'</a>';
    }
}
