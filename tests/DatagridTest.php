<?php
namespace Tests;

use Facebook\WebDriver\WebDriverBy as By;

/**
 * DatagridTest : testing datagrid behaviour (MRA, sorting, ...)
 *
 * ToDo:
 * Sort : sorting on relations on view pages
 */
class DatagridTest extends AtkTestCase
{
    /**
     * Custom action
     */
    public function testCustomAction()
    {
        $employee1 = $this->db->addEmployee([':salary' => 1730]);
        $employee2 = $this->db->addEmployee([':salary' => 1730]);

        $this->login();
        // Raising the salary for the first employee
        $this->goToEmployee('admin');
        $dg = $this->parseDataGrid();
        $dg[$employee1[':name']]['Actions']['Raise salary']->click();

        $this->goToEmployee('admin');
        $dg = $this->parseDataGrid();
        $this->assertEquals('€ 1,880.00', $dg[$employee1[':name']]['Salary']->getText());
        $this->assertEquals('€ 1,730.00', $dg[$employee2[':name']]['Salary']->getText());
    }

    /**
     * MRA Simplest case : MRA on admin page
     */
    public function testMRADelete()
    {
        $toDelete = [$this->db->addEmployee(), $this->db->addEmployee(), $this->db->addEmployee()];
        $toKeep = [$this->db->addEmployee(), $this->db->addEmployee()];

        $this->login();
        $this->goToEmployee('admin', '', 'App.employeeRelation');
        $dg = $this->parseDataGrid();
        foreach ($toDelete as $employee) {
            $dg[$employee[':name']]['Checkbox']->click();
        }
        $this->wd->findElement(By::cssSelector('input[value="Delete"]'))->click();
        $this->wd->findElement(By::cssSelector('input[value="Yes"]'))->click();
        $this->goToEmployee('admin', '', 'App.employeeRelation');

        $employeeNames = array_keys($this->parseDataGrid());
        foreach ($toDelete as $employee) {
            $this->assertNotContains($employee[':name'], $employeeNames);
        }
        foreach ($toKeep as $employee) {
            $this->assertContains($employee[':name'], $employeeNames);
        }
    }

    /**
     * MRA from relation page
     */
    public function testMRADeleteRelation()
    {
        $department = $this->db->addDepartment();
        $employeeConfig = [':department' => $department[':id']];
        $toDelete = [
            $this->db->addEmployee($employeeConfig),
            $this->db->addEmployee($employeeConfig),
            $this->db->addEmployee($employeeConfig)
        ];
        $toKeep = [$this->db->addEmployee($employeeConfig), $this->db->addEmployee($employeeConfig)];

        $this->login();
        $this->goToDepartment('edit', $department[':id']);
        $dg = $this->parseDataGrid('App_department_employees_grid');
        foreach ($toDelete as $employee) {
            $dg[$employee[':name']]['Checkbox']->click();
        }
        $this->wd->findElement(By::cssSelector('input[value="Delete"]'))->click();
        $this->wd->findElement(By::cssSelector('input[value="Yes"]'))->click();

        $this->goToDepartment('edit', $department[':id']);
        $employeeNames = array_keys($this->parseDataGrid('App_department_employees_grid'));
        foreach ($toDelete as $employee) {
            $this->assertNotContains($employee[':name'], $employeeNames);
        }
        foreach ($toKeep as $employee) {
            $this->assertContains($employee[':name'], $employeeNames);
        }
    }

    /**
     * MRA with complex primary keys
     */
    public function testMRADeleteComplexPrimaryKey()
    {
        $toDelete = [$this->db->addProject(), $this->db->addProject(), $this->db->addProject()];
        $toKeep = [$this->db->addProject(), $this->db->addProject()];

        $this->login();
        $this->goToProject('admin');
        $dg = $this->parseDataGrid();
        foreach ($toDelete as $project) {
            $dg[$project[':name']]['Checkbox']->click();
        }
        $this->wd->findElement(By::cssSelector('input[value="Delete"]'))->click();
        $this->wd->findElement(By::cssSelector('input[value="Yes"]'))->click();
        $this->goToProject('admin');

        $projectNames = array_keys($this->parseDataGrid());
        foreach ($toDelete as $project) {
            $this->assertNotContains($project[':name'], $projectNames);
        }
        foreach ($toKeep as $project) {
            $this->assertContains($project[':name'], $projectNames);
        }
    }

    /**
     * MRA with a custom action
     */
    public function testMRACustomAction()
    {
        $employee1 = $this->db->addEmployee([':salary' => 1730]);
        $employee2 = $this->db->addEmployee([':salary' => null]);
        $employee3 = $this->db->addEmployee([':salary' => 1730]);

        $this->login();
        // Raising the salary for the first 2 employees
        $this->goToEmployee('admin');
        $dg = $this->parseDataGrid();
        $dg[$employee1[':name']]['Checkbox']->click();
        $dg[$employee2[':name']]['Checkbox']->click();
        $this->select2('admin_atkaction', 'Raise salary');
        $this->wd->findElement(By::cssSelector('input[value="Go"]'))->click();

        $this->goToEmployee('admin');
        $dg = $this->parseDataGrid();
        $this->assertEquals('€ 1,880.00', $dg[$employee1[':name']]['Salary']->getText());
        $this->assertEquals('€ 150.00', $dg[$employee2[':name']]['Salary']->getText());
        $this->assertEquals('€ 1,730.00', $dg[$employee3[':name']]['Salary']->getText());
    }

    /**
     * Verify that on the datagrid $names are in the given order
     *
     * each $name is found amongst titles of columns
     *
     * @param array $names
     */
    private function checkOrder($names, $htmlId = 'admin', $indexColumn = 'Name')
    {
        // Computing the $name => $index in datagrid Array :
        $found = array_keys($this->parseDataGrid($htmlId, $indexColumn));
        $indexes = array_combine($found, range(0, count($found)-1));

        for ($i = 0, $max = count($names) - 2; $i <= $max; $i++) {
            $this->assertGreaterThan($indexes[$names[$i]], $indexes[$names[$i+1]]);
        }
    }

    /**
     * Testing default Order set with setOrder
     */
    public function testSortDefault()
    {
        $employee1 = $this->db->addEmployee([':name' => 'Albert']);
        $employee3 = $this->db->addEmployee([':name' => 'Chad']);
        $employee2 = $this->db->addEmployee([':name' => 'Barbara']);

        $this->login();

        $this->goToEmployee();
        $this->checkOrder([$employee1[':name'], $employee2[':name'], $employee3[':name']]);
    }

    /**
     * Testing re-ordering
     */
    public function testSortAdmin()
    {
        $department1 = $this->db->addDepartment([':name' => 'Assistance']);
        $department3 = $this->db->addDepartment([':name' => 'Communication']);
        $department2 = $this->db->addDepartment([':name' => 'Bakery']);

        $this->login();

        // Default order : by id / creation order
        $this->goToDepartment();
        $this->checkOrder([$department1[':name'], $department3[':name'], $department2[':name']]);

        // Order by name (ASC)
        $this->wd->findElement(By::linkText('Name'))->click();
        $this->checkOrder([$department1[':name'], $department2[':name'], $department3[':name']]);

        // Order by name (DESC)
        $this->wd->findElement(By::linkText('Name'))->click();
        $this->checkOrder([$department3[':name'], $department2[':name'], $department1[':name']]);
    }

    /**
     * Test ordering on O2O Relation
     **/
    public function testSortAdminO2ORelation()
    {
        $address1 = $this->db->addAddress([':city' => 'Atlanta']);
        $address2 = $this->db->addAddress([':city' => 'Berkeley']);
        $address3 = $this->db->addAddress([':city' => 'Chicago']);
        $employee1 = $this->db->addEmployee([':address' => $address1[':id']]);
        $employee2 = $this->db->addEmployee([':address' => $address2[':id']]);
        $employee3 = $this->db->addEmployee([':address' => $address3[':id']]);

        $this->login();

        $this->goToEmployee('admin', 0, 'App.employeeRelation');
        $this->wd->findElement(By::linkText('Address'))->click();
        $this->checkOrder([$employee1[':name'], $employee2[':name'], $employee3[':name']]);
    }

    /**
     * Test ordering on M2O Relation
     **/
    public function testSortAdminM2ORelation()
    {
        $manager1 = $this->db->addEmployee([':name' => 'Albert']);
        $manager2 = $this->db->addEmployee([':name' => 'Barbara']);
        $manager3 = $this->db->addEmployee([':name' => 'Chad']);
        $department1 = $this->db->addDepartment([':name' => 'Assistance']);
        $department2 = $this->db->addDepartment([':name' => 'Bakery']);
        $department3 = $this->db->addDepartment([':name' => 'Communication']);
        $employee1 = $this->db->addEmployee([':manager' => $manager1[':id'], ':department' => $department1[':id']]);
        $employee2 = $this->db->addEmployee([':manager' => $manager2[':id'], ':department' => $department2[':id']]);
        $employee3 = $this->db->addEmployee([':manager' => $manager3[':id'], ':department' => $department3[':id']]);

        $this->login();
        $this->goToEmployee('admin', 0, 'App.employeeRelation');

        // Test with manager relation (ASC/DESC)
        $this->wd->findElement(By::linkText('Manager'))->click();
        $this->checkOrder([$employee1[':name'], $employee2[':name'], $employee3[':name']]);
        $this->wd->findElement(By::linkText('Manager'))->click();
        $this->checkOrder([$employee3[':name'], $employee2[':name'], $employee1[':name']]);

        // Test with department relation (ASC/DESC)
        $this->wd->findElement(By::linkText('Department'))->click();
        $this->checkOrder([$employee1[':name'], $employee2[':name'], $employee3[':name']]);
        $this->wd->findElement(By::linkText('Department'))->click();
        $this->checkOrder([$employee3[':name'], $employee2[':name'], $employee1[':name']]);
    }

    /**
     * Test ordering on UTF8 column with space in field name
     **/
    public function testSortWithSpace()
    {
        if (getenv('DB_DRIVER') == 'mysql') {
            return;
        }
        $book1 = $this->db->addBook([':name' => 'Hamlet', ':number2' => 159]);
        $book2 = $this->db->addBook([':name' => 'Madame Bovary', ':number2' => 10]);
        $book3 = $this->db->addBook([':name' => 'War and Peace', ':number2' => 17]);

        $this->login();
        $this->goToBook('admin');

        $this->wd->findElement(By::linkText('Ex ़ॏॠ༛ᇻቡᙻ⏏⏰⑰漣年'))->click();
        $this->checkOrder([$book2[':name'], $book3[':name'], $book1[':name']], 'admin', '"title" ?');
    }

    /**
     * Test ordering on UTF8 relation
     */
    public function testSortUTF8Relation()
    {
        if (getenv('DB_DRIVER') == 'mysql') {
            return;
        }
        $parent1 = $this->db->addBook([':name' => 'Hamlet']);
        $parent2 = $this->db->addBook([':name' => 'Madame Bovary']);
        $parent3 = $this->db->addBook([':name' => 'War and Peace']);
        $book1 = $this->db->addBook([':parent' => $parent1[':id']]);
        $book2 = $this->db->addBook([':parent' => $parent2[':id']]);
        $book3 = $this->db->addBook([':parent' => $parent3[':id']]);

        $this->login();
        $this->goToBook('admin');

        $this->wd->findElement(By::linkText('↺'))->click();
        $this->checkOrder([$book1[':name'], $book2[':name'], $book3[':name']], 'admin', '"title" ?');
        $this->wd->findElement(By::linkText('↺'))->click();
        $this->checkOrder([$book3[':name'], $book2[':name'], $book1[':name']], 'admin', '"title" ?');
    }

    /**
     * Test ordering on datagrid in view page
     */
    public function testSortViewO2MRelation()
    {
        $department = $this->db->addDepartment();
        $employee1 = $this->db->addEmployee([':name' => 'Albert', ':department' => $department[':id']]);
        $employee2 = $this->db->addEmployee([':name' => 'Barbara', ':department' => $department[':id']]);
        $employee3 = $this->db->addEmployee([':name' => 'Chad', ':department' => $department[':id']]);

        $this->login();
        $this->goToDepartment('view', $department[':id']);

        $this->wd->findElements(By::linkText('Name'))[0]->click();
        $this->checkOrder(
            [$employee3[':name'], $employee2[':name'], $employee1[':name']],
            'App_department_employees_grid'
        );
        $this->wd->findElements(By::linkText('Name'))[0]->click();
        $this->checkOrder(
            [$employee1[':name'], $employee2[':name'], $employee3[':name']],
            'App_department_employees_grid'
        );
    }

    /**
     * Test ordering on datgrid in view page (relation with UTF8 chars)
     */
    public function testSortViewO2MRelationUTF8()
    {
        if (getenv('DB_DRIVER') == 'mysql') {
            return;
        }
        $parent = $this->db->addBook();
        $book1 = $this->db->addBook([':name' => 'Hamlet', ':parent' => $parent[':id']]);
        $book2 = $this->db->addBook([':name' => 'Madame Bovary', ':parent' => $parent[':id']]);
        $book3 = $this->db->addBook([':name' => 'War and Peace', ':parent' => $parent[':id']]);

        $this->login();
        $this->goToBook('view', $parent[':id']);

        $this->wd->findElement(By::linkText('"title" ?'))->click();
        $this->checkOrder(
            [$book1[':name'], $book2[':name'], $book3[':name']],
            'App_book_Child_books_____14714ad6_grid',
            '"title" ?'
        );
        $this->wd->findElement(By::linkText('"title" ?'))->click();
        $this->checkOrder(
            [$book3[':name'], $book2[':name'], $book1[':name']],
            'App_book_Child_books_____14714ad6_grid',
            '"title" ?'
        );
    }
}
