<?php

namespace Tests;

use Lmc\Steward\Test\AbstractTestCase;
use Facebook\WebDriver\WebDriverBy as By;
use Facebook\WebDriver\WebDriverExpectedCondition;
use Facebook\WebDriver\WebDriverKeys;
use Facebook\WebDriver\WebDriverSelect as Select;

/**
 * Main test class with helper functions. Real tests should extend this class.
 */
abstract class AtkTestCase extends AbstractTestCase
{
    /**
     * @var Db
     */
    public $db;
    /**
     * @var string address of tested application
     */
    public $appAddress;

    /**
     * __construct : reads parameters.APP_ENV.php to build database connection.
     */
    public function __construct()
    {
        // Setting up the DB connection :
        $this->db = new Db();
        $this->appAddress = 'http://'.getenv('APP_SERVER');
    }

    /**
     * Login with credentials given
     *
     * @param string $username to log in with
     * @param string $password to log in with
     *
     * @return true if login has succeeded, false if not.
     */
    protected function login($username = Db::DEFAULT_USERNAME, $password = Db::DEFAULT_PASSWORD)
    {
        $this->wd->get($this->appAddress.'/index.php?atklogout=1');

        // Put credentials
        $this->wd->findElement(By::cssSelector('input[type="text"]'))->sendKeys($username);
        $this->wd->findElement(By::cssSelector('input[type="password"]'))->sendKeys($password);
        $this->wd->findElement(By::name('login'))->click();

        // If no error found, then we succeeded
        return !$this->hasErrorOnPage();
    }

    /**
     * Check if there is an error message in current page
     *
     * @return true if there is an error
     */
    protected function hasErrorOnPage()
    {
        return count($this->findMultipleByCss('.alert')) != 0;
    }

    /**
     * Go to specified page
     *
     * @param string $node        in 'module.node' notation
     * @param string $action      like 'admin', 'add', 'edit', ...
     * @param string $atkselector for specifying which node on view/edit pages
     */
    protected function goTo($node, $action = 'admin', $atkselector = '')
    {
        $this->wd->get("{$this->appAddress}/index.php?atknodeuri={$node}&atkaction={$action}{$atkselector}");
    }

    /**
     * Go to employee page
     *
     * @param string $action like 'admin', 'add', 'edit', ...
     * @param string $id     of the employee
     * @param string $node   'App.employee' or 'App.employeeRelations'
     */
    protected function goToEmployee($action = 'admin', $id = '', $node = 'App.employee')
    {
        $atkselector = '';
        if ($id != '') {
            $atkselector = "&atkselector=app_employee.id%3D'{$id}'";
        }
        $this->goTo($node, $action, $atkselector);
    }

    /**
     * Go to department page
     *
     * @param string $action like 'admin', 'add', 'edit', ...
     * @param string $id     of the department
     */
    protected function goToDepartment($action = 'admin', $id = '')
    {
        $atkselector = '';
        if ($id != '') {
            $atkselector = "&atkselector=app_department.id%3D'{$id}'";
        }
        $this->goTo('App.department', $action, $atkselector);
    }

    /**
     * Go to project page
     *
     * @param string $action    like 'admin', 'add', 'edit', ...
     * @param string $name      of the project
     * @param string $startdate of the project
     */
    protected function goToProject($action = 'admin', $name = '', $startdate = '')
    {
        $atkselector = '';
        if ($name != '') {
            $atkselector = "&atkselector=app_project.name%3D'{$name}'+AND+app_project.startdate%3D'{$startdate}'";
        }
        $this->goTo('App.project', $action, $atkselector);
    }

    /**
     * Go to furniture page
     *
     * @param string $action like 'admin', 'add', 'edit', ...
     * @param string $id     of the furniture
     */
    protected function goToFurniture($action = 'admin', $id = '')
    {
        $atkselector = '';
        if ($id != '') {
            $atkselector = "&atkselector=app_furniture.id%3D'{$id}'";
        }
        $this->goTo('App.furniture', $action, $atkselector);
    }

    /**
     * Go to Book page
     *
     * @param string $action like 'admin', 'add', 'edit', ...
     * @param string $id     of the furniture
     */
    protected function goToBook($action = 'admin', $id = '')
    {
        $atkselector = '';
        if ($id != '') {
            $atkselector = "&atkselector=app_book.id%3D'{$id}'";
        }
        $this->goTo('App.book', $action, $atkselector);
    }

    /**
     * Select a value for a select2 box in a form (JS-way)
     *
     * @param string $name of the select
     * @param string $option to select (the displayed value, not the internal one)
     */
    protected function select2($name, $option)
    {
        $script = <<<JS
var sel = document.getElementsByName('{$name}')[0];
for (let option of sel.options) {
    if(option.innerHTML=="{$option}") {
        sel.value = option.value;
    }
}
JS;
        $this->wd->executeScript($script);
    }

    /**
     * Select a value for a select2 box with autocomplete search
     *
     * @param string $name of the select
     * @param string|array $values to find and select
     */
    protected function select2Keyboard($name, $values)
    {
        if (!is_array($values)) {
            $values = [$values];
        }
        foreach ($values as $value) {
            $this->wd->findElement(By::xpath("//select[@id='{$name}']/following-sibling::span[1]"))->click();
            $this->wd->switchTo()->activeElement()->clear()->sendKeys($value);
            $this->wd->wait(5, 200)->until(WebDriverExpectedCondition::elementTextContains(
                By::id("select2-{$name}-results"),
                $value
            ));
            $this->wd->findElement(By::className('select2-results__option--highlighted'))->click();
            $this->wd->wait(5, 200)->until(
                WebDriverExpectedCondition::invisibilityOfElementLocated(By::id("select2-{$name}-results"))
            );
        }
    }

    /**
     * Builds array of labels => values(view)|fields (edit) in view or edit pages
     *
     * @return array string ("label") => WebDriverElement ("field")
     */
    protected function parseNodePage()
    {
        $result = [];
        foreach ($this->wd->findElements(By::className('section-item')) as $row) {
            try {
                $result[$row->findElement(By::className('col-sm-2'))->getText()] =
                    $row->findElement(By::className('col-sm-10'));
            } catch (\Facebook\WebDriver\Exception\NoSuchElementException $e) {
            }
        }
        return $result;
    }

    /**
     * Finds a datagrid and returns an array indexed by $indexColumn values with
     * rows actions and data.
     *
     * @param string $htmlId      where to find the datagrid
     * @param string $indexColumn column to use for the index of return value
     *
     * @return bool|array colValues => [
        'Actions' => [
            'View' => WebDriverElement(icon),
            'Edit' => WebDriverElement(icon)],
        'Checkbox' => WebDriverElement (checkbox, if exists)
        'Col2' => WebDriverElement(td),
        'Col3' => WebDriverElement(td),
        ...
     * ]
     */
    protected function parseDataGrid($htmlId = 'admin', $indexColumn = 'Name')
    {
        try {
            $dg = $this->wd->findElement(By::id($htmlId));
            $rows = $dg->findElements(By::tagName('tr'));
        } catch (\Facebook\WebDriver\Exception\NoSuchElementException $e) {
            return false;
        }
        // Getting column names :
        if (!count($rows) or $rows[0]->getAttribute('class') != 'recordList-header-row') {
            return false;
        }
        $index = 0;
        $subIndexes = [];
        foreach ($rows[0]->findElements(By::tagName('th')) as $i => $column) {
            $colTitle = trim($column->getText());
            if ($colTitle == $indexColumn) {
                $index = $i;
            }
            if ($colTitle != '') {
                $indexes[$i] = $colTitle;
            }
        }
        array_shift($rows);

        // Stripping of search row if needed :
        if ($rows[0]->getAttribute('class') == 'recordList-search-row') {
            array_shift($rows);
        }

        // Now exploiting data
        $result = [];
        foreach ($rows as $row) {
            $columns = $row->findElements(By::tagName('td'));
            if (count($columns) < $index or count($columns) < max($indexes)) {
                continue;
            }
            $rowIndex = $columns[$index]->getText();
            $result[$rowIndex]['Actions'] = [];
            foreach ($row->findElements(By::tagName('i')) as $icon) {
                $result[$rowIndex]['Actions'][$icon->getAttribute('title')] = $icon;
            }
            $checkboxes = $row->findElements(By::cssSelector('input[type="checkbox"]'));
            if (count($checkboxes)) {
                $result[$rowIndex]['Checkbox'] = $checkboxes[0];
            }
            foreach ($indexes as $i => $title) {
                $result[$rowIndex][$title] = $columns[$i];
            }
        }
        return $result;
    }

    /**
     * Set date in a ATK form
     *
     * @param string $name of the field
     * @param array $date  to set [0=>Y, 1=>m, 2=>d]
     */
    protected function setDate($name, $date)
    {
        $year = $this->wd->findElement(By::name($name.'[year]'));
        $year->clear();
        // Editing date field (need to redo some operations for this to work ... i don't know why !)
        $year->sendKeys($date[0]);
        (new Select($this->wd->findElement(By::name($name.'[month]'))))->selectByValue((int)$date[1]);
        (new Select($this->wd->findElement(By::name($name.'[day]'))))->selectByValue((int)$date[2]);
        (new Select($this->wd->findElement(By::name($name.'[month]'))))->selectByValue((int)$date[1]);
        $year->sendKeys($date[0]);
    }

    /**
     * Set time in a ATK form, like setDate
     *
     * @param string $name of the field
     * @param array $time  to set [0=>H, 1=>i, 2=>s]
     */
    protected function setTime($name, $time)
    {
        (new Select($this->wd->findElement(By::name($name.'[hours]'))))->selectByValue($time[0]);
        (new Select($this->wd->findElement(By::name($name.'[minutes]'))))->selectByValue($time[1]);
        if (isset($time[2])) {
            (new Select($this->wd->findElement(By::name($name.'[seconds]'))))->selectByValue($time[1]);
        }
    }

    /**
     * Ensure the desired checkbox is (un)checked
     *
     * @param WebDriverElement $checkbox to control status
     * @param boolean $check true if you want it to be checked,
     *                      false if you want it to be unchecked
     */
    protected function setCheckbox($elem, $check = true)
    {
        if ($check xor $elem->getAttribute('checked') !== null) {
            $elem->click();
        }
    }
}
