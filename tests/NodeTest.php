<?php
namespace Tests;

use Facebook\WebDriver\WebDriverBy as By;

/**
 * LoginTest : testing node properties (for the moment : eventlog)
 */
class NodeTest extends AtkTestCase
{
    public function testEventLog()
    {
        // Adding a user that belong to sampleGroup
        $user = $this->db->addUser([], [1]);
        $employee = $this->db->addEmployee();
        $this->login($user[':username'], Db::DEFAULT_PASSWORD);

        // Generating a line in the logs :
        $this->goToEmployee('view', $employee[':id']);

        $this->goTo('App.log', 'admin');
        $this->select2Keyboard('App_log_userid_AE_userid', $user[':username']);
        $this->wd->findElement(By::cssSelector('input[value="Search"]'))->click();

        $found = $this->parseDataGrid();
        $found = array_pop($found);
        $this->assertContains('App.employee', $found['Node']->getText());
        $this->assertEquals('view', $found['Action']->getText());
    }

    /**
     * Test the application of fuzzy filter on node
     */
    public function testFilterFuzzy()
    {
        $employee1 = $this->db->addEmployee([':name' => 'Zhu', ':vacation' => true, ':age' => 30]);
        $employee2 = $this->db->addEmployee([':name' => 'Ali', ':vacation' => true, ':age' => 61]);

        $this->login();

        $this->goToEmployee('admin', '', 'App.employeeFilter');

        $found = array_keys($this->parseDataGrid());
        $this->assertNotContains($employee1[':name'], $found);
        $this->assertContains($employee2[':name'], $found);
    }

    /**
     * Test the application of key-value filter on node
     */
    public function testFilterKeyValue()
    {
        $employee1 = $this->db->addEmployee([':name' => 'Elizabeth', ':vacation' => true, ':age' => 59]);
        $employee2 = $this->db->addEmployee([':name' => 'Bernie', ':vacation' => false, ':age' => 61]);

        $this->login();

        $this->goToEmployee('admin', '', 'App.employeeFilter');

        $found = array_keys($this->parseDataGrid());
        $this->assertContains($employee1[':name'], $found);
        $this->assertNotContains($employee2[':name'], $found);
    }
}
