<?php
namespace Tests;

use Facebook\WebDriver\WebDriverBy as By;
use Facebook\WebDriver\WebDriverSelect as Select;

/**
 * TreeNodeTest : testing behaviour of tree nodes.
 */
class TreeNodeTest extends AtkTestCase
{

    /**
     * Parses the tree table to extract lines and actions
     *
     * @return array [name1 =>
     *   ['Toggle' => webDriverElement,
     *    'New' => webDriverElement,
     *    'Edit' => webDriverElement,
     *    'Delete' => webDriverElement],
     *   name2 => [...]]
     */
    private function parseTreeTable()
    {
        $result = [];
        foreach ($this->wd->findElements(By::tagName('tr')) as $row) {
            // Discarding empty row
            if (trim($row->getText()) == '') {
                continue;
            }
            $name = $row->findElement(By::cssSelector('td[colspan]'))->getText();
            $result[$name] = [];
            $links = $row->findElements(By::tagName('a'));
            foreach ($links as $link) {
                $linkTitle = $link->getText();
                if (in_array($linkTitle, ['New', 'Edit', 'Delete'])) {
                    $result[$name][$linkTitle] = $link;
                } else {
                    $result[$name]['Toggle'] = $link;
                }
            }
        }
        return $result;
    }

    /**
     * Check that the toggle button displays/hides children.
     */
    public function testToggleParent()
    {
        $manager = $this->db->addEmployee();
        $employee = $this->db->addEmployee([':manager' => $manager[':id']]);
        $this->login();
        
        $this->goToEmployee('admin', '', 'App.employeeTree');
        $table = $this->parseTreeTable();
        // Initially, the children node should not be visible, so let's make it visible :
        if (!in_array($employee[':name'], array_keys($table))) {
            $table[$manager[':name']]['Toggle']->click();
            $table = $this->parseTreeTable();
        }

        $this->assertContains($employee[':name'], array_keys($table));
        // Now, let's try to hide children :
        $table[$manager[':name']]['Toggle']->click();
        $table = $this->parseTreeTable();
        $this->assertNotContains($employee[':name'], array_keys($table));
    }

    /**
     * Check that the toggle button displays/hides all descendants.
     */
    public function testToggleGrandParent()
    {
        $director = $this->db->addEmployee();
        $manager = $this->db->addEmployee([':manager' => $director[':id']]);
        $employee = $this->db->addEmployee([':manager' => $manager[':id']]);
        $this->login();
        
        $this->goToEmployee('admin', '', 'App.employeeTree');
        $table = $this->parseTreeTable();
        // Let's ensure manager and employee are visible :
        if (!in_array($manager[':name'], array_keys($table))) {
            $table[$director[':name']]['Toggle']->click();
            $table = $this->parseTreeTable();
        }
        if (!in_array($employee[':name'], array_keys($table))) {
            $table[$manager[':name']]['Toggle']->click();
            $table = $this->parseTreeTable();
        }

        $this->assertContains($employee[':name'], array_keys($table));
        // Now, let's try to hide descendants :
        $table[$director[':name']]['Toggle']->click();
        $table = $this->parseTreeTable();
        $this->assertNotContains($employee[':name'], array_keys($table));
    }

    /**
     * Ensure that when adding a child node, it is listed under parent one
     */
    public function testAddChild()
    {
        $manager = $this->db->addEmployee();
        $employeeName = 'William ' . $this->db->randomString();
        $this->login();

        $this->goToEmployee('admin', '', 'App.employeeTree');
        $table = $this->parseTreeTable();
        $table[$manager[':name']]['New']->click();
        $this->wd->findElement(By::name('name'))->clear()->sendKeys($employeeName);
        $this->wd->findElement(By::cssSelector('button[type="submit"]'))->click();
        $this->db->deferQuery('DELETE FROM `app_employee` WHERE `name` = ?', [$employeeName]);

        $this->goToEmployee('admin', '', 'App.employeeTree');
        $table = $this->parseTreeTable();
        if (!in_array($employeeName, array_keys($table))) {
            $table[$manager[':name']]['Toggle']->click();
            $table = $this->parseTreeTable();
        }

        $this->assertContains($employeeName, array_keys($table));
    }

    /**
     * Check that we can change a node position
     */
    public function testChangeParent()
    {
        $manager1 = $this->db->addEmployee();
        $manager2 = $this->db->addEmployee();
        $employee = $this->db->addEmployee([':manager' => $manager1[':id']]);
        $this->login();

        $this->goToEmployee('admin', '', 'App.employeeTree');
        $table = $this->parseTreeTable();
        // Let's ensure employee is visible :
        if (!in_array($employee[':name'], array_keys($table))) {
            $table[$manager1[':name']]['Toggle']->click();
            $table = $this->parseTreeTable();
        }
        $table[$employee[':name']]['Edit']->click();

        (new Select($this->wd->findElement(By::name('manager'))))->selectByVisiblePartialText($manager2[':name']);
        $this->wd->findElement(By::cssSelector('button[type="submit"]'))->click();

        $this->goToEmployee('admin', '', 'App.employeeTree');
        $table = $this->parseTreeTable();
        $initiallyDisplayed = in_array($employee[':name'], array_keys($table));
        $table[$manager2[':name']]['Toggle']->click();
        $table = $this->parseTreeTable();
        $this->assertTrue(in_array($employee[':name'], array_keys($table)) == !$initiallyDisplayed);
    }

    /**
     * Ensure that when deleting a parent, descedants are also removed
     */
    public function testRecursiveDelete()
    {
        $director = $this->db->addEmployee();
        $manager = $this->db->addEmployee([':manager' => $director[':id']]);
        $employee = $this->db->addEmployee([':manager' => $manager[':id']]);
        $this->login();

        $this->goToEmployee('admin', '', 'App.employeeTree');
        $table = $this->parseTreeTable();
        $table[$director[':name']]['Delete']->click();
        $this->wd->findElement(By::cssSelector('input[value="Yes"]'))->click();

        $this->goToEmployee('admin');
        $employeeNames = array_keys($this->parseDataGrid());
        $this->assertNotContains($director[':name'], $employeeNames);
        $this->assertNotContains($manager[':name'], $employeeNames);
        $this->assertNotContains($employee[':name'], $employeeNames);
    }
}
