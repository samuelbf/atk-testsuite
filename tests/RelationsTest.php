<?php
namespace Tests;

use Facebook\WebDriver\WebDriverBy as By;
use Facebook\WebDriver\WebDriverSelect as Select;

/**
 * RelationsTest : testing relations between nodes
 */
class RelationsTest extends AtkTestCase
{
    /**
     * @var array target address definition (different from default one in Db->addAddress).
     */
    private static $sampleAddress = [
            ':address' => '1600 Amphitheatre Parkway',
            ':zipcode' => 'CA 94043',
            ':city' => 'Mountain View',
        ];

    /**
     * Test that values render correctly for OneToOne and ManyToOne relations
     */
    public function testXToOneDisplay()
    {
        $this->login();
        $address = $this->db->addAddress(self::$sampleAddress);
        $department = $this->db->addDepartment();
        $employee = $this->db->addEmployee([':address' => $address[':id'], ':department' => $department[':id']]);

        $expected = $address;
        $expected[':department'] = $department[':name'];
        $this->checkSampleRelationsValues($expected, $employee[':id']);
    }

    /**
     * Test that values can be rightfully edited for OneToOne and ManyToOne
     */
    public function testXToOneEdit()
    {
        $this->login();
        $address_id = $this->db->addAddress()[':id'];
        $employee = $this->db->addEmployee([':address' => $address_id]);
        $department = $this->db->addDepartment();
        $expected = self::$sampleAddress;
        $expected[':department'] = $department[':name'];

        // Editing values
        $this->goToEmployee('edit', $employee[':id'], 'App.employeeRelation');
        $this->wd->findElement(By::name('address_AE_address'))->clear()->sendKeys($expected[':address']);
        $this->wd->findElement(By::name('address_AE_zipcode'))->clear()->sendKeys($expected[':zipcode']);
        $this->wd->findElement(By::name('address_AE_city'))->clear()->sendKeys($expected[':city']);
        $this->select2('department', $expected[':department']);
        $this->wd->findElement(By::cssSelector('button[type="submit"]'))->click();

        $this->checkSampleRelationsValues($expected, $employee[':id']);
    }

    /**
     * Test that a value can be added in a one-to-one relation
     */
    public function testOneToOneAdd()
    {
        $department = $this->db->addDepartment();
        $employee = $this->db->addEmployee([':department' => $department[':id']]);
        $expected = self::$sampleAddress;
        $expected[':department'] = $department[':name'];

        $this->login();
        $this->gotoEmployee('edit', $employee[':id'], 'App.employeeRelation');
        $this->wd->findElement(By::name('address_AE_address'))->clear()->sendKeys($expected[':address']);
        $this->wd->findElement(By::name('address_AE_zipcode'))->clear()->sendKeys($expected[':zipcode']);
        $this->wd->findElement(By::name('address_AE_city'))->clear()->sendKeys($expected[':city']);
        $this->wd->findElement(By::cssSelector('button[type="submit"]'))->click();

        $this->checkSampleRelationsValues($expected, $employee[':id']);
    }

    /**
     * Check that displayed values for sample address corresponds to
     * expected ones.
     *
     * This function is called by testXToOneDisplay/Edit
     */
    private function checkSampleRelationsValues($expected, $employee_id)
    {
        $this->goToEmployee('view', $employee_id, 'App.employeeRelation');
        $displayedValues = $this->parseNodePage();

        $this->assertEquals($expected[':address'], $displayedValues['Address']->getText());
        $this->assertEquals($expected[':zipcode'], $displayedValues['Zipcode']->getText());
        $this->assertEquals($expected[':city'], $displayedValues['City']->getText());
        $this->assertContains($expected[':department'], $displayedValues['Department']->getText());
    }

    /**
     * Test link on relation name in datagrid ("admin") page
     */
    public function testManyToOneLinkAdmin()
    {
        $department = $this->db->addDepartment();
        $employee = $this->db->addEmployee([':department' => $department[':id']]);

        $this->login();
        $this->goToEmployee('admin', 0, 'App.employeeRelation');
        $dg = $this->parseDataGrid();
        
        $dg[$employee[':name']]['Department']->findElement(By::tagName('a'))->click();
        $this->assertContains($department[':name'], $this->wd->getTitle());
    }

    /**
     * Test link on relation name in view page
     */
    public function testManyToOneLinkView()
    {
        $department = $this->db->addDepartment();
        $employee = $this->db->addEmployee([':department' => $department[':id']]);

        $this->login();
        $this->goToEmployee('view', $employee[':id'], 'App.employeeRelation');

        $this->parseNodePage()['Department']->findElement(By::tagName('a'))->click();
        $this->assertContains($department[':name'], $this->wd->getTitle());
    }

    /**
     * Test view link aside relation select in edit page
     */
    public function testManyToOneLinkEditView()
    {
        $department = $this->db->addDepartment();
        $employee = $this->db->addEmployee([':department' => $department[':id']]);

        $this->login();
        $this->goToEmployee('edit', $employee[':id'], 'App.employeeRelation');

        $this->parseNodePage()['Department']->findElement(By::linkText('View'))->click();
        $this->assertContains($department[':name'], $this->wd->getTitle());
    }

    /**
     * Test new link aside relation select in edit page
     */
    public function testManyToOneLinkEditNew()
    {
        $employee = $this->db->addEmployee();
        $departmentName = 'HR '.$this->db->randomString();

        $this->login();
        $this->goToEmployee('edit', $employee[':id'], 'App.employeeRelation');
        
        // Editing department
        $this->parseNodePage()['Department']->findElement(By::linkText('New'))->click();
        $this->wd->findElement(By::name('name'))->clear()->sendKeys($departmentName);
        // Saving department form
        $this->db->deferQuery('DELETE FROM app_department WHERE name = ?', [$departmentName]);
        $this->wd->findElement(By::cssSelector('button[type="submit"]'))->click();
        // Saving employee form
        $this->wd->findElement(By::cssSelector('button[type="submit"]'))->click();

        $this->goToEmployee('view', $employee[':id'], 'App.employeeRelation');
        $this->assertContains($departmentName, $this->parseNodePage()['Department']->getText());
    }

    /**
     * Test the select autocomplete for ManyToOne relations
     */
    public function testManyToOneAutocomplete()
    {
        $manager = $this->db->addEmployee([':name' => 'Jenny']);
        $employee = $this->db->addEmployee();

        $this->login();
        $this->goToEmployee('edit', $employee[':id'], 'App.employeeRelation');
        $this->select2Keyboard('App_employeeRelation_manager', $manager[':name']);
        // Saving employee form
        $this->wd->findElement(By::cssSelector('button[type="submit"]'))->click();

        $this->goToEmployee('view', $employee[':id'], 'App.employeeRelation');
        $this->assertContains($manager[':name'], $this->parseNodePage()['Manager']->getText());
    }

    /**
     * Test displayed grid in OneToMany relation and view link to target node
     */
    public function testOneToManyDisplay()
    {
        $department = $this->db->addDepartment();
        $employee1 = $this->db->addEmployee([':department' => $department[':id']]);
        $employee2 = $this->db->addEmployee([':department' => $department[':id']]);

        $this->login();
        $this->goToDepartment('view', $department[':id']);
        $dg = $this->parseDataGrid('App_department_employees_grid');
        $names = array_keys($dg);

        // Testing that we found the same names:
        $this->assertCount(2, $names);
        $this->assertContains($employee1[':name'], $names);
        $this->assertContains($employee2[':name'], $names);

        // Testing that we can go to target node (we pick the last row):
        $dg[$employee1[':name']]['Actions']['View']->click();
        $this->assertContains($employee1[':name'], $this->wd->getTitle());
    }

    /**
     * Test the addition of a target element in OneToMany relations
     */
    public function testOneToManyAdd()
    {
        $department = $this->db->addDepartment();
        $this->login();
        $this->goToDepartment('edit', $department[':id']);
        $employeeName = 'Clara C ' . $this->db->randomString();

        // Clicking the 'Add' button:
        $this->wd->findElement(By::linkText('Add'))->click();
        // Editing/submitting form
        $this->wd->findElement(By::name('name'))->sendKeys($employeeName);
        $this->wd->findElement(By::cssSelector('button[type="submit"]'))->click();
        $this->db->deferQuery('DELETE FROM app_employee WHERE name = ?', [$employeeName]);

        $names = array_keys($this->parseDataGrid('App_department_employees_grid'));

        // Testing that we found the same names:
        $this->assertContains($employeeName, $names);
    }

    /**
     * Test the edition of a target element in OneToMany relations
     */
    public function testOneToManyEdit()
    {
        $department = $this->db->addDepartment();
        $employee = $this->db->addEmployee([':department' => $department[':id']]);
        $this->login();
        $this->goToDepartment('edit', $department[':id']);
        $newName = 'Reza K ' . $this->db->randomString();

        // Finding the edit button in the datagrid :
        $grid = $this->parseDataGrid('App_department_employees_grid');
        $grid[$employee[':name']]['Actions']['Edit']->click();

        // Editing/submitting form
        $this->wd->findElement(By::name('name'))->clear()->sendKeys($newName);
        $this->wd->findElement(By::cssSelector('button[type="submit"]'))->click();

        $grid = $this->wd->findElement(By::id('App_department_employees_grid'));

        // Testing that we found the same names:
        $this->assertContains($newName, $grid->getText());
    }

    /**
     * Test that ManyToMany targets appear in view page
     */
    public function testManyToManyDisplayAndLink()
    {
        $employee1 = $this->db->addEmployee([':name' => 'Bonnie']);
        $employee2 = $this->db->addEmployee([':name' => 'Clyde']);
        $project = $this->db->addProject([], [$employee1[':id'], $employee2[':id']]);

        $this->login();
        $this->goToProject('view', $project[':name'], $project[':startdate']);

        $members = $this->parseNodePage()['Members'];
        // Testing the list of members :
        $this->assertContains($employee1[':name'], $members->getText());
        $this->assertContains($employee2[':name'], $members->getText());

        // Clicking on one member :
        $members->findElement(By::linkText($employee1[':name']))->click();
        $this->assertContains($employee1[':name'], $this->wd->getTitle());
    }

    /**
     * Test that ManyToMany can be edited
     */
    public function testManyBoolAddAndRemove()
    {
        $employee1 = $this->db->addEmployee([':name' => 'Bonnie']);
        $employee2 = $this->db->addEmployee([':name' => 'Clyde']);
        $project = $this->db->addProject([], [$employee1[':id']]);

        $this->login();
        $this->goToProject('edit', $project[':name'], $project[':startdate']);
        $members = $this->parseNodePage()['Members'];
        foreach ($members->findElements(By::tagName('input')) as $checkbox) {
            // Checking employee2, unchecking employee1
            if ($checkbox->getAttribute('value') == $employee1[':id']) {
                $checkbox->click();
            }
            if ($checkbox->getAttribute('value') == $employee2[':id']) {
                $checkbox->click();
            }
        }
        $this->wd->findElement(By::cssSelector('button[type="submit"]'))->click();

        $this->goToProject('view', $project[':name'], $project[':startdate']);
        $members = $this->parseNodePage()['Members'];
        // Testing the list of members :
        $this->assertNotContains($employee1[':name'], $members->getText());
        $this->assertContains($employee2[':name'], $members->getText());
    }

    /**
     * Test edition of a ManyToMany select relation
     */
    public function testManyToManySelectEdit()
    {
        $employee1 = $this->db->addEmployee([':name' => 'Mary']);
        $employee2 = $this->db->addEmployee([':name' => 'Ronnie']);
        $furniture = $this->db->addFurniture([':name' => 'chair'], [$employee2[':id']]);
        $this->login();

        // edit :
        $this->goToFurniture('edit', $furniture[':id']);
        $this->parseNodePage()['Holders']->findElement(By::linkText('Remove'))->click();
        $this->select2Keyboard('App_furniture_holders_m2msr_add', $employee1[':name']);
        $this->wd->findElement(By::cssSelector('button[type="submit"]'))->click();

        // check final values :
        $this->goToFurniture('view', $furniture[':id']);
        $holders = $this->parseNodePage()['Holders']->getText();
        $this->assertContains($employee1[':name'], $holders);
        $this->assertNotContains($employee2[':name'], $holders);
    }

    /**
     * Test edition of a Shuttle relation
     */
    public function testManyToManyShuttleEdit()
    {
        $employee = $this->db->addEmployee();
        $furniture1 = $this->db->addFurniture([':name' => 'chalks'], [$employee[':id']]);
        $furniture2 = $this->db->addFurniture([':name' => 'pencils']);
        $this->login();

        // edit :
        $this->goToEmployee('edit', $employee[':id'], 'App.employeeRelation');
        $controls = $this->wd->findElement(By::className('shuttle-controls'));
        (new Select($this->wd->findElement(By::name('furniture[][furniture]'))))->selectByValue($furniture1[':id']);
        $controls->findElement(By::className('glyphicon-triangle-left'))->click();
        (new Select($this->wd->findElement(By::name('furniture_sel'))))->selectByValue($furniture2[':id']);
        $controls->findElement(By::className('glyphicon-triangle-right'))->click();
        $this->wd->findElement(By::cssSelector('button[type="submit"]'))->click();

        // check final values :
        $this->goToEmployee('view', $employee[':id'], 'App.employeeRelation');
        $furnitures = $this->parseNodePage()['Furniture']->getText();
        $this->assertNotContains($furniture1[':name'], $furnitures);
        $this->assertContains($furniture2[':name'], $furnitures);
    }

    /**
     * Test relations descriptor handlers as functions
     */
    public function testRelationDescriptorHandler()
    {
        $department1 = $this->db->addDepartment([':name' => 'Security']);
        $employee1 = $this->db->addEmployee([':department' => $department1[':id'], ':email' => 'jen@ex.us']);
        $employee2 = $this->db->addEmployee([':department' => $department1[':id'], ':email' => null]);
        $department2 = $this->db->addDepartment([':name' => 'Logistics']);

        $this->login();
        $this->goToDepartment('admin');

        $grid = $this->parseDataGrid();
        $department1Mails = $grid[$department1[':name']]['Emails']->getText();
        $this->assertContains($employee1[':email'], $department1Mails);
        $this->assertNotContains($employee1[':name'], $department1Mails);
        $this->assertContains('No mail set !', $department1Mails);
        $this->assertNotContains('No mail set !', $grid[$department2[':name']]['Emails']->getText());
    }

    /**
     * Test relations descriptors templates
     */
    public function testRelationDescriptorTemplate()
    {
        $address = $this->db->addAddress([':zipcode' => $this->db->randomString(), ':city' => 'New Orleans']);
        $department = $this->db->addDepartment([':name' => 'Security']);
        $employee = $this->db->addEmployee([':department' => $department[':id'], ':address' => $address[':id']]);

        $this->login();
        $this->goToDepartment('admin');

        $grid = $this->parseDataGrid();
        $this->assertContains($address[':zipcode'], $grid[$department[':name']]['Addresses']->getText());
    }

    /**
     * Test the destination filter on a relation
     */
    public function testRelationDestinationFilterTemplate()
    {
        $department1 = $this->db->addDepartment([':statuses' => 'will_be_closed_soon|need_to_change_boss']);
        $department2 = $this->db->addDepartment([':statuses' => 'need_to_change_boss']);
        $employee = $this->db->addEmployee([':name' => 'April', ':email' => 'will_be_closed']);

        $this->login();

        // Check that employee1 is not in the initial list
        $this->goToEmployee('edit', $employee[':id'], 'App.employeeRelation');
        $dptOpts = $this->wd->findElement(By::id('App_employeeRelation_department'))
            ->findElements(By::tagName('option'));
        foreach ($dptOpts as $option) {
            $displayed[] = $option->getText();
        }
        $this->assertNotContains($department1[':name'], $displayed);
        $this->assertContains($department2[':name'], $displayed);

        // Check that when we release the filter, both departments appear :
        $this->wd->findElement(By::id('App_employeeRelation_email'))->clear()->sendKeys('m@pr.il');
        $this->wd->findElement(By::cssSelector('button[type="submit"]'))->click();
        $this->goToEmployee('edit', $employee[':id'], 'App.employeeRelation');
        $dptOpts = $this->wd->findElement(By::id('App_employeeRelation_department'))
            ->findElements(By::tagName('option'));
        foreach ($dptOpts as $option) {
            $displayed[] = $option->getText();
        }
        $this->assertContains($department1[':name'], $displayed);
        $this->assertContains($department2[':name'], $displayed);
    }

    /**
     * Test dependency code for M2M select relation when adding a row
     */
    public function testManyToManySelectRelationAddDependency()
    {
        $employee1 = $this->db->addEmployee([':name' => 'Donald']);
        $employee2 = $this->db->addEmployee([':name' => 'Daniel']);
        $furniture = $this->db->addFurniture([':name' => 'scissors'], [$employee2[':id']]);

        $this->login();
        $this->goToFurniture('edit', $furniture[':id']);
        $warning = $this->wd->findElement(By::id('App_furniture_Warning'));

        // First check that warning is not displayed :
        $this->assertFalse($warning->isDisplayed());

        // Then check that when Donald is added, the warning is displayed :
        $this->select2Keyboard('App_furniture_holders_m2msr_add', $employee1[':name']);
        $this->assertTrue($warning->isDisplayed());
    }

    /**
     * Test dependency code for M2M select relation when deleting a row
     */
    public function testManyToManySelectRelationRemoveDependency()
    {
        $employee = $this->db->addEmployee([':name' => 'Donald']);
        $furniture = $this->db->addFurniture([':name' => 'scissors'], [$employee[':id']]);

        $this->login();
        $this->goToFurniture('edit', $furniture[':id']);
        $warning = $this->wd->findElement(By::id('App_furniture_Warning'));

        // First check that warning is displayed :
        $this->assertTrue($warning->isDisplayed());

        // Then check that when Donald is removed, the warning is hidden :
        $this->parseNodePage()['Holders']->findElement(By::linkText('Remove'))->click();
        $this->assertFalse($warning->isDisplayed());
    }
}
