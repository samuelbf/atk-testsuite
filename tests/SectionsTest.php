<?php
namespace Tests;

use Facebook\WebDriver\WebDriverBy as By;

/**
 * SectionsTest: testing sections & tabs display/hide and positions
 */
class SectionsTest extends AtkTestCase
{
    /**
     * Check that the section is initially expanded as expected
     */
    public function testSectionDefaultExpanded()
    {
        $employee = $this->db->addEmployee();

        $this->login();
        $this->goToEmployee('edit', $employee[':id'], 'App.employeeSections');

        $this->assertTrue($this->wd->findElement(By::name('name'))->isDisplayed());
    }

    /**
     * Check that we can toggle section
     */
    public function testSectionToggle()
    {
        $employee = $this->db->addEmployee();

        $this->login();
        $this->goToEmployee('edit', $employee[':id'], 'App.employeeSections');
        $toggleLink = $this->wd->findElement(By::partialLinKText('HR'));
        $salary = $this->wd->findElement(By::name('salary'));
        $initiallyDisplayed = $salary->isDisplayed();

        $toggleLink->click();
        $this->assertTrue($salary->isDisplayed() == !$initiallyDisplayed);
        $toggleLink->click();
        $this->assertTrue($salary->isDisplayed() == $initiallyDisplayed);
    }

    /**
     * Check that clicking tab title has the desired effect
     */
    public function testChangeTab()
    {
        $employee = $this->db->addEmployee();

        $this->login();
        $this->goToEmployee('edit', $employee[':id'], 'App.employeeSections');
        $color = $this->wd->findElement(By::name('color'));

        $this->wd->findElement(By::linkText('General'))->click();
        $this->assertFalse($color->isDisplayed());
        $this->wd->findElement(By::linkText('Some details'))->click();
        $this->assertTrue($color->isDisplayed());
    }

    /**
     * Check clicking tab title in a tabbed pane
     */
    public function testChangeTabInTabbedPane()
    {
        $employee = $this->db->addEmployee();

        $this->login();
        $this->goToEmployee('edit', $employee[':id'], 'App.employeeSections');
        $createdBy = $this->wd->findElement(By::cssSelector('label[for="App_employeeSections_createdby"]'));
        $updatedBy = $this->wd->findElement(By::cssSelector('label[for="App_employeeSections_updatedby"]'));
        $this->wd->findElement(By::linkText('Some details'))->click();

        $this->wd->findElement(By::linkText('Creation information'))->click();
        $this->assertTrue($createdBy->isDisplayed());
        $this->assertFalse($updatedBy->isDisplayed());
        $this->wd->findElement(By::linkText('Update information'))->click();
        $this->assertFalse($createdBy->isDisplayed());
        $this->assertTrue($updatedBy->isDisplayed());
    }

    /**
     * Check the explicit ordering of tab
     */
    public function testTabOrder()
    {
        $employee = $this->db->addEmployee();

        $this->login();
        $this->goToEmployee('view', $employee[':id'], 'App.employeeSections');

        $tabLinks = $this->wd->findElements(By::cssSelector('.nav-tabs a'));
        $this->assertEquals('First tab', $tabLinks[0]->getText());
        $this->assertEquals('Some details', $tabLinks[1]->getText());
        $this->assertEquals('General', $tabLinks[2]->getText());
    }

    /**
     * Check that an attribute can be present in several tabs
     */
    public function testAttributeSeveralTabs()
    {
        $employee = $this->db->addEmployee();

        $this->login();
        $this->goToEmployee('edit', $employee[':id'], 'App.employeeSections');
        $age = $this->wd->findElement(By::id('App_employeeSections_age'));

        $this->wd->findElement(By::linkText('Some details'))->click();
        $this->assertFalse($age->isDisplayed());
        $this->wd->findElement(By::linkText('General'))->click();
        $this->assertTrue($age->isDisplayed());
        $this->wd->findElement(By::linkText('First tab'))->click();
        $this->assertTrue($age->isDisplayed());
    }

    /**
     * Check that an attribute can be present in all tabs
     */
    public function testAttributeAllTabs()
    {
        $employee = $this->db->addEmployee();

        $this->login();
        $this->goToEmployee('edit', $employee[':id'], 'App.employeeSections');
        $blog = $this->wd->findElement(By::id('App_employeeSections_blog'));

        $this->wd->findElement(By::linkText('Some details'))->click();
        $this->assertTrue($blog->isDisplayed());
        $this->wd->findElement(By::linkText('General'))->click();
        $this->assertTrue($blog->isDisplayed());
        $this->wd->findElement(By::linkText('First tab'))->click();
        $this->assertTrue($blog->isDisplayed());
    }

    /**
     * Check that if we expand a section, it remains expanded in other pages
     */
    public function testSectionPersistence()
    {
        $employee1 = $this->db->addEmployee();
        $employee2 = $this->db->addEmployee();

        $this->login();
        $this->goToEmployee('view', $employee1[':id'], 'App.employeeSections');
        // Expanding HR section
        $this->wd->findElement(By::partialLinkText('HR'))->click();

        $this->goToEmployee('edit', $employee2[':id'], 'App.employeeSections');
        $this->assertTrue($this->wd->findElement(By::id('App_employeeSections_notes'))->isDisplayed());
    }

    /**
     * Check that if we choose a tab, it remains selected in other pages
     */
    public function testTabPersistence()
    {
        $employee1 = $this->db->addEmployee();
        $employee2 = $this->db->addEmployee();

        $this->login();
        $this->goToEmployee('view', $employee1[':id'], 'App.employeeSections');
        // Expanding HR section
        $this->wd->findElement(By::linkText('Some details'))->click();

        $this->goToEmployee('edit', $employee2[':id'], 'App.employeeSections');
        $this->assertTrue($this->wd->findElement(By::id('App_employeeSections_color'))->isDisplayed());
    }

    /**
     * Check that the tabs from One-to-one relation integrates well
     */
    public function testTabFromRelation()
    {
        $employee = $this->db->addEmployee();

        $this->login();
        $this->goToEmployee('edit', $employee[':id'], 'App.employeeSections');

        $this->wd->findElement(By::linkText('City'))->click();
        $this->assertTrue($this->wd->findElement(By::id('App_addressSections_address_AE_zipcode'))->isDisplayed());
    }

    /**
     * Selecting the tab that only appears in add form lead default tab in other forms
     */
    public function testTabDefaultFallback()
    {
        $employee = $this->db->addEmployee();

        $this->login();
        $this->goToEmployee('add', null, 'App.employeeSections');
        $this->wd->findElement(By::linkText('Part-time tab'))->click();

        $this->goToEmployee('edit', $employee[':id'], 'App.employeeSections');
        $activeTabs = $this->wd->findElements(By::className('activetab'));
        $this->assertNotEmpty('General', $activeTabs);
        $this->assertEquals('General', $activeTabs[0]->getText());
    }
}
