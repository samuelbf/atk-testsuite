<?php
namespace Tests;

use Facebook\WebDriver\WebDriverSelect as Select;
use Facebook\WebDriver\WebDriverBy as By;

/**
 * AttributesTest : testing attributes and their properties
 */
class AttributesTest extends AtkTestCase
{
    /**
     * @var array target employee definition (different from default one in Db->addEmployee).
     */
    private static $sampleEmployee = [
            ':gender' => 'O',
            ':name' => 'Mich K',
            ':email' => 'paulb@corp.com',
            ':color' => '#cd2d46',
            ':age' => 37,
            ':department' => null,
            ':hiredate' => '2018-09-12 14:00:00',
            ':vacation' => true,
            ':notes' => "Nice guy.\nBut a bit lazy...",
            ':blog' => 'https://github.com/Sintattica/atk/',
            ':qualities' => 41, // nice, generous, encouraging
            ':salary' => 1462.24773693,
            ':manager' => null,
            ':createdby' => 1,
            ':createstamp' => '2019-01-01 18:03:43',
            ':updatedby' => 1,
            ':updatestamp' => '2019-02-15 14:37:08',
        ];

    /**
     * Testing obligatory field
     */
    public function testObligatoryField()
    {
        $this->login();
        $this->goToEmployee('add');
        $this->wd->findElement(By::cssSelector('button[type="submit"]'))->click();

        $this->assertTrue($this->hasErrorOnPage());
    }

    /**
     * Testing unique field
     */
    public function testUniqueField()
    {
        $this->login();
        $name = $this->db->addEmployee([':name' => 'Paul'])[':name'];
        $this->goToEmployee('add');
        $this->wd->findElement(By::name('name'))->sendKeys($name);
        $this->wd->findElement(By::cssSelector('button[type="submit"]'))->click();

        $this->assertTrue($this->hasErrorOnPage());
    }

    /**
     * Convert a date to the format displayed by Atk
     *
     * @param string $date in the date('Y-m-d H:i:s) format
     *
     * @return string date('d/m/Y H:i');
     */
    private function dateTime2Html($date)
    {
        $chunks = explode(' ', $date);
        $date = explode('-', $chunks[0]);
        $time = explode(':', $chunks[1]);
        return $date[2].'/'.$date[1].'/'.$date[0].' '.$time[0].':'.$time[1];
    }

    /**
     * Testing how attributes are displayed on 'view' pages
     *
     * TODO : test more attribute types
     */
    public function testAttributesDisplay()
    {
        $this->login();
        $employee = $this->db->addEmployee(self::$sampleEmployee);
        $employee[':createdby'] = 'sampleUser';
        $employee[':updatedby'] = 'sampleUser';

        $this->checkSampleEmployeeValues($employee);
    }

    /**
     * Testing edition of attributes on edit pages
     *
     * TODO : test more attribute types
     */
    public function testAttributesEdit()
    {
        $user = $this->db->addUser([], [1]);
        $this->login($user[':username']);
        $employee = self::$sampleEmployee;
        $old_employee = $this->db->addEmployee([':createstamp' => $employee[':createstamp']]);
        $employee[':id'] = $old_employee[':id'];
        $employee[':name'] = $old_employee[':name'];

        // Editing employee
        $this->goToEmployee('edit', $employee[':id']);
        $this->select2('gender', $employee[':gender']);
        $this->wd->findElement(By::name('name'))->clear()->sendKeys($employee[':name']);
        $this->wd->findElement(By::name('email'))->clear()->sendKeys($employee[':email']);
        $this->wd->findElement(By::name('age'))->clear()->sendKeys($employee[':age']);
        $vacation = $this->wd->findElement(By::name('vacation'));
        $this->setCheckbox($this->wd->findElement(By::name('vacation')), $employee[':vacation']);
        $this->wd->findElement(By::name('notes'))->clear()->sendKeys($employee[':notes']);
        for ($i = 0; $i <= 6; $i++) {
            $this->setCheckbox(
                $this->wd->findElement(By::cssSelector('input[name="qualities[]"][value="'.(1<<$i).'"]')),
                (1<<$i)&$employee[':qualities']
            );
        }
        $this->wd->findElement(By::name('notes'))->clear()->sendKeys($employee[':notes']);
        $this->wd->findElement(By::name('blog'))->clear()->sendKeys($employee[':blog']);
        $this->wd->findElement(By::name('salary'))->clear()->sendKeys($employee[':salary']);
        $this->setDate('hiredate', ['2018', '9', '12']);
        $this->setTime('hiredate', ['14', '00']);
        $this->wd->findElement(By::name('color'))->clear()->sendKeys($employee[':color']);
        $this->wd->findElement(By::cssSelector('button[type="submit"]'))->click();
        $employee[':updatestamp'] = date('Y-m-d H:i:s');
        $employee[':createdby'] = 'sampleUser';
        $employee[':updatedby'] = $user[':username'];

        $this->checkSampleEmployeeValues($employee);
    }

    /**
     * Checks that displayed values for sample employee corresponds to
     * expected ones.
     *
     * This function is called by testAttributesDisplay/Edit
     */
    private function checkSampleEmployeeValues($employee)
    {
        $this->goToEmployee('view', $employee[':id']);
        $displayedValues = $this->parseNodePage();

        $this->assertEquals($employee[':gender'], $displayedValues['Gender']->getText());
        $this->assertEquals($employee[':name'], $displayedValues['Name']->getText());
        $this->assertContains(
            "<a href=\"mailto:{$employee[':email']}\">{$employee[':email']}</a>",
            $displayedValues['Email']->getAttribute('innerHTML')
        );
        $this->assertEquals($employee[':age'], $displayedValues['Age']->getText());
        $this->assertEquals($employee[':color'], $displayedValues['Color']->getText());
        $this->assertContains('GDPR-compliant', $displayedValues['Gdpr']->getText());
        $this->assertEquals($this->dateTime2Html($employee[':hiredate']), $displayedValues['Hiredate']->getText());
        $this->assertEquals($employee[':vacation'] ? 'Yes' : 'No', $displayedValues['Vacation']->getText());
        $notes = $displayedValues['Notes']->getAttribute('innerHTML');
        $this->assertContains('Nice guy.', $notes);
        $this->assertContains('<br', $notes);
        $this->assertContains('But a bit lazy...', $notes);
        $this->assertNotEmpty($displayedValues['Blog']->findElements(By::partialLinkText('Sintattica/atk')));
        $qualities = $displayedValues['Qualities']->getText();
        $this->assertContains('Nice', $qualities);
        $this->assertContains('Generous', $qualities);
        $this->assertContains('Encouraging', $qualities);
        $this->assertEquals('€ 1,462.25', $displayedValues['Salary']->getText());
        $totalSalary =
            (new \DateTime())->diff(new \DateTime(explode(' ', $employee[':hiredate'])[0]))->days * 1462.25 / 30;
        $this->assertEquals(round($totalSalary), round($displayedValues['Total salary']->getText()));
        $this->assertEquals($employee[':createdby'], $displayedValues['Createdby']->getText());
        $this->assertEquals(
            $this->dateTime2Html($employee[':createstamp']),
            $displayedValues['Createstamp']->getText()
        );
        $this->assertEquals($employee[':updatedby'], $displayedValues['Updatedby']->getText());
        $this->assertEquals(
            $this->dateTime2Html($employee[':updatestamp']),
            $displayedValues['Updatestamp']->getText()
        );
    }

    /**
     * Testing numeric attributes (NumberAttribute, CurrencyAttribute)
     */
    public function testNumericConstraints()
    {
        $name = 'Dorothy '.Db::randomString();
        $this->login();

        $this->goToEmployee('add');
        $this->wd->findElement(By::name('name'))->sendKeys($name);
        $this->wd->findElement(By::name('age'))->sendKeys('strVal');
        $this->wd->findElement(By::cssSelector('button[type="submit"]'))->click();
        $this->assertTrue($this->hasErrorOnPage());

        $this->goToEmployee('add');
        $this->wd->findElement(By::name('name'))->sendKeys($name);
        $this->wd->findElement(By::name('age'))->sendKeys('-45');
        $this->wd->findElement(By::cssSelector('button[type="submit"]'))->click();
        $this->assertTrue($this->hasErrorOnPage());

        $this->goToEmployee('add');
        $this->wd->findElement(By::name('name'))->sendKeys($name);
        $this->wd->findElement(By::name('salary'))->sendKeys('strVal');
        $this->wd->findElement(By::cssSelector('button[type="submit"]'))->click();
        $this->assertTrue($this->hasErrorOnPage());
    }

    /**
     * Checks the displayed aggregated column
     *
     * Note: source code suggest that we could reference relation attributes with dot
     * notation, but it fails when I try to. A feature to implement ?
     */
    public function testAggregatedColumnDisplay()
    {
        $employee = $this->db->addEmployee(
            [':age' => 61, ':color' => '#a82db9', ':notes' => "Really impressive.\nKeep it as long as possible."]
        );
        $this->login();

        $this->goToEmployee();
        $grid = $this->parseDataGrid();
        $displayed = $grid[$employee[':name']]['Characteristics']->getAttribute('innerHTML');

        $this->assertContains((string)$employee[':age'], $displayed);
        $this->assertContains($employee[':color'], $displayed);
        $this->assertContains('Really impressive', $displayed);
        $this->assertContains('<br', $displayed);
        $this->assertContains('Keep it', $displayed);
    }

    /**
     * Check the ajax update of an attribute triggered by another attribute
     */
    public function testDependencyRefresh()
    {
        $this->login();

        $this->goToEmployee('add');

        $this->wd->findElement(By::name('color'))->clear();
        $this->wd->findElement(By::name('age'))->clear()->sendKeys('62');
        $this->wd->findElement(By::name('name'))->click();
        $color = $this->wd->findElement(By::name('color'))->getAttribute('value');

        $this->assertEquals('#9e611e', $color);
    }

    /**
     * Check initial values
     */
    public function testInitialValues()
    {
        $this->login();

        $this->goToEmployee('add');

        // Test for boolean attribute
        $vacation = $this->wd->findElement(By::name('vacation'));
        $this->assertEquals('true', $vacation->getAttribute('checked'));

        // Test for currency (number) attributes
        $salary = $this->wd->findElement(By::name('salary'));
        $this->assertEquals('2000.00', $salary->getAttribute('value'));

        // Test for flag attribute
        $nice = $this->wd->findElement(By::cssSelector('input[name="qualities[]"][value="'.(1<<0).'"]'));
        $this->assertEquals('true', $nice->getAttribute('checked'));
        $encouraging = $this->wd->findElement(By::cssSelector('input[name="qualities[]"][value="'.(1<<5).'"]'));
        $this->assertEquals('true', $nice->getAttribute('checked'));
    }
}
