<?php
namespace Tests;

use Facebook\WebDriver\WebDriverBy as By;
use Facebook\WebDriver\WebDriverSelect as Select;

/**
 * StackTest : testing session stack
 */
class StackTest extends AtkTestCase
{
    /**
     *  Test that the stack preserve datagrid order
     */
    public function testKeepDatagridOrder()
    {
        $employee1 = $this->db->addEmployee([':name' => 'Albert']);
        $employee2 = $this->db->addEmployee([':name' => 'Aretha']);
        $this->login();

        $this->goToEmployee('admin', '', 'App.employeeRelation');
        // Sort by name :
        $this->wd->findElements(By::linkText('Name'))[0]->click();
        $datagrid = $this->parseDataGrid();
        $names = array_keys($datagrid);
        $initialOrder =
            array_search($employee1[':name'], $names) > array_search($employee2[':name'], $names);

        // Going one level further in stack
        $datagrid[$employee1[':name']]['Actions']['View']->click();
        // And back :
        $this->wd->findElements(By::cssSelector('ol.breadcrumb a'))[0]->click();

        $names = array_keys($this->parseDataGrid());
        $this->assertEquals(
            $initialOrder,
            array_search($employee1[':name'], $names) > array_search($employee2[':name'], $names)
        );
    }

    /**
     * Test that the stack preserve form edits
     */
    public function testStackedEdit()
    {
        $employee = $this->db->addEmployee([':name' => 'Albert']);
        $newEmail = 'a.mcgill@localhost.loc';
        $this->login();

        $this->goToEmployee('edit', $employee[':id'], 'App.employeeRelation');
        // Edit email :
        $this->wd->findElement(By::name('email'))->clear()->sendKeys($newEmail);

        // Going one level further in stack ('new department')
        $this->wd->findElements(By::linkText('New'))[0]->click();
        // And back :
        $this->wd->findElement(By::name('atkcancel'))->click();

        $this->assertEquals($newEmail, $this->wd->findElement(By::name('email'))->getAttribute('value'));
    }

    /**
     * Test that the stack GC doesn't clean up current stack
     *
     * @warning slow test (it waits enough time for the GC to run)
     */
    public function testWaitAndBack()
    {
        $employee = $this->db->addEmployee([':name' => 'Albert']);
        $this->login();

        $this->goToEmployee('admin');
        // Goi one level further in stack & wait
        $this->parseDataGrid()[$employee[':name']]['Actions']['View']->click();
        sleep(7);

        // Come back :
        $this->wd->findElements(By::cssSelector('ol.breadcrumb a'))[0]->click();

        // Check that we are really on "admin" page (if GC drops stack, we land on application welcoming page)
        $this->assertNotEmpty($this->wd->findElements(By::id('admin')));
    }
}
