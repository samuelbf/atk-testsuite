<?php
namespace Tests;

/**
 * Db: a wrapper around PDO with Mysql driver and sensible options.
 *
 * @category Tests
 * @package  AtkTestSuite
 * @author   Samuel BF <>
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL, version 3
 * @link     https://www.github.com/Samuel-BF/atktestsuite/
 */
class Db
{
    /**
     * @var \PDO connection item
     */
    public $conn;
    /**
     * @var array $query => $parameters of clean-up queries that should happen
     *            at the end of the test
     */
    private $deferredQueries = [];
    /**
     * @var string default username
     */
    const DEFAULT_USERNAME = 'sampleUser';
    /**
     * @var string default password
     */
    const DEFAULT_PASSWORD = 'Qp+qsC^@=41';
    /**
     * @var string hash corresponding to this password
     */
    const DEFAULT_HASH = '$2y$10$SwXvKhKCYmQ3eFKVcFr9.O5R2zBS2k4LfGmttrjU0dRoDwYnNUgCW';

    /**
     * Connects to the database
     */
    public function __construct()
    {
        $driver = getenv('DB_DRIVER') ?? 'mysql';
        if ($driver == 'sqlite') {
            $this->conn = new \PDO('sqlite:'.getenv('DB_FILE'));
            return;
        }
        // Else... (mysql or pgsql)
        $host = getenv('DB_HOST') ?? 'localhost';
        $database = getenv('DB_DATABASE') ?? 'atktestsuite';
        $charset = $driver == 'mysql' ? ';charset=utf8mb4' : '';
        $this->conn = new \PDO(
            "{$driver}:host={$host};dbname={$database}{$charset}",
            getenv('DB_USER'),
            getenv('DB_PASSWORD')
        );
        if ($driver == 'mysql') {
            $this->conn->query('SET sql_mode = "ANSI"');
        }
    }

    /**
     * Execute a prepared query against the database
     *
     * @param string $query      to execute (with '?' as placeholders)
     * @param array  $parameters array of parameters value
     *
     * @return PDOStatement
     */
    public function query($query, $parameters = [])
    {
        static $pdoTypes = [
            'integer' => \PDO::PARAM_INT,
            'boolean' => \PDO::PARAM_BOOL,
            'NULL' => \PDO::PARAM_NULL,
            'string' => \PDO::PARAM_STR,
        ];
        $stmt = $this->conn->prepare($query);
        if (!$stmt) {
            throw new \ErrorException("Fail to prepare {$query} : " . $this->conn->errorInfo()[2]);
        }
        $numericIndex = gettype(array_keys($parameters)[0] ?? 'str') == 'integer' ? true : false;
        foreach ($parameters as $key => $value) {
            $stmt->bindValue(
                $numericIndex ? $key + 1 : $key,
                $value,
                $pdoTypes[gettype($value)] ?? \PDO::PARAM_STR
            );
        }
        if (!$stmt->execute()) {
            throw new \ErrorException("Fail to execute {$query} : " . $stmt->errorInfo()[2]);
        }
        return $stmt;
    }

    /**
     * Defer a query to be executed at the end of the test (clean-up queries)
     *
     * @param string $query      to execute (with '?' as placeholders)
     * @param array  $parameters array of parameters value
     */
    public function deferQuery($query, $parameters = [])
    {
        $this->deferredQueries[] = [$query, $parameters];
    }

    /**
     * Add a line inside $table and removes it after the test
     *
     * This function is called by addUser and addEmployee (and maybe some
     * other in the future)
     *
     * @param array $values in ':colname' => 'value' format
     *
     * @return array $values with ':id' set to corresponding id
     */
    private function addItem($table, $values)
    {
        $stripColon = function ($x) {
                return substr($x, 1);
        };
        $colNames = array_map($stripColon, array_keys($values));
        $this->query(
            'INSERT INTO '.$table.'("'.implode($colNames, '","').'") '.
            'VALUES ('.implode(', ', array_keys($values)).')',
            $values
        );
        $values[':id'] = $this->conn->lastInsertId();
        $this->deferQuery('DELETE FROM '.$table.' WHERE id = ?', [$values[':id']]);

        return $values;
    }

    /**
     * Create an user inside the database with password self::DEFAULT_PASSWORD
     *
     * @param array $user   fields to override defaults.
     * @param array $groups (id) that user should be added to.
     *
     * @return array $user fields with id and real username (unique)
     */
    public function addUser($user = [], $groups = [])
    {
        $defaultOptions = [
            ':username' => 'sampleUser',
            ':passwd' => self::DEFAULT_HASH,
            ':firstname' => 'Paul',
            ':lastname' => 'McCartney',
            ':email' => 'paul@example.net',
            ':isDisabled' => false,
            ':isAdmin' => false,
            ':isU2FEnabled' => false,
        ];
        $options = array_merge($defaultOptions, $user);
        $options[':username'] .= ' ' . $this->randomString();
        $user = $this->addItem('auth_users', $options);

        // Adding user to groups :
        foreach ($groups as $group) {
            $this->query(
                'INSERT INTO auth_users_groups(user_id, group_id) VALUES(?, ?)',
                [$user[':id'], $group]
            );
            $this->deferQuery(
                'DELETE FROM auth_users_groups WHERE user_id = ? AND group_id = ?',
                [$user[':id'], $group]
            );
        }

        return $user;
    }

    /**
     * Create an employee inside the database
     *
     * @param array $employee fields to override defaults.
     *
     * @return array $employee fields with id
     */
    public function addEmployee($employee = [])
    {
        $defaultOptions = [
            ':name' => 'John',
            ':gender' => 'M',
            ':email' => 'john@corp.com',
            ':age' => 49,
            ':color' => null,
            ':address' => null,
            ':department' => null,
            ':hiredate' => date('Y-m-d H:i:s'),
            ':vacation' => false,
            ':notes' => 'Some sample notes',
            ':blog' => 'https://www.owasp.org',
            ':qualities' => 70,
            ':salary' => '2512.67',
            ':manager' => null,
            ':createdby' => 1,
            ':createstamp' => date('Y-m-d H:i:s'),
            ':updatedby' => 1,
            ':updatestamp' => date('Y-m-d H:i:s'),
        ];
        $options = array_merge($defaultOptions, $employee);
        $options[':name'] .= ' '.$this->randomString();
        return $this->addItem('app_employee', $options);
    }

    /**
     * Create a department
     *
     * @param array $options fields to override defaults.
     *
     * @return array $options fields with id and real name (random)
     */
    public function addDepartment($options = [])
    {
        $defaultOptions = [
            ':name' => 'Support',
            ':statuses' => 'is_hiring',
            ':manager' => null,
        ];
        $options = array_merge($defaultOptions, $options);
        $options[':name'] .= ' '.$this->randomString();
        return $this->addItem('app_department', $options);
    }

    /**
     * Add an address inside the database
     *
     * @param array $address fields to override defaults.
     *
     * @return array $address fields with id
     */
    public function addAddress($address = [])
    {
        $defaultOptions = [
            ':address' => '1600 Pennsylvania Avenue',
            ':zipcode' => 'DC 20500',
            ':city' => 'Washington',
        ];
        $options = array_merge($defaultOptions, $address);
        return $this->addItem('app_address', $options);
    }

    /**
     * Add a project inside the database with random name
     *
     * @param array $project  fields to override defaults.
     * @param array $members  of the project (ids)
     *
     * @return array $project fields with id and real name
     */
    public function addProject($project = [], $members = [])
    {
        $defaultOptions = [
            ':name' => 'Rocket',
            ':startdate' => date('Y-m-d'),
            ':description' => 'Will go to Mars. If it works.',
        ];
        $options = array_merge($defaultOptions, $project);
        $options[':name'] .= ' '.$this->randomString();
        $this->query(
            'INSERT INTO "app_project"("name", "startdate", "description") VALUES (:name, :startdate, :description)',
            $options
        );
        $this->deferQuery(
            'DELETE FROM "app_project" WHERE "name" = :name AND "startdate" = :startdate',
            [':name' => $options[':name'], ':startdate' => $options[':startdate']]
        );
        foreach ($members as $id) {
            $this->query(
                'INSERT INTO "app_project_employee"("name", "startdate", "employee") VALUES (:name, :startdate, :id)',
                [':name' => $options[':name'], ':startdate' => $options[':startdate'], ':id' => $id]
            );
        }
        $this->deferQuery(
            'DELETE FROM "app_project_employee" WHERE "name" = :name AND "startdate" = :startdate',
            [':name' => $options[':name'], ':startdate' => $options[':startdate']]
        );
        return $options;
    }

    /**
     * Add furnitures and give them to selected employees
     *
     * @param array $furniture field (name) to override default
     * @param array $employees who will be given these (as id)
     *
     * @return array $furniture with id and real name
     */
    public function addFurniture($furniture = [], $employees = [])
    {
        $defaultOption = [':name' => ['Red', 'Black', 'Blue', 'Green'][rand(0, 3)]. ' pencil'];
        $options = array_merge($defaultOption, $furniture);
        $options[':name'] .= ' '.$this->randomString();
        $furniture = $this->addItem('app_furniture', $options);

        // Adding furniture to employees :
        foreach ($employees as $granted) {
            $this->query(
                'INSERT INTO app_employee_furniture(employee, furniture) VALUES(?, ?)',
                [$granted, $furniture[':id']]
            );
            $this->deferQuery(
                'DELETE FROM app_employee_furniture WHERE employee = ? AND furniture = ?',
                [$granted, $furniture[':id']]
            );
        }
        return $furniture;
    }

    /**
     * Add a book
     *
     * @param array $options fields to override defaults.
     *
     * @return array $options fields with id and real name (random)
     */
    public function addBook($options = [])
    {
        $defaultOptions = [
            ':name' => '1984',
            ':author' => 'Georges Orwell',
            ':date' => '08-06-1949',
            ':number1' => '173',
            ':number2' => '1984',
            ':parent' => null,
        ];
        $options = array_merge($defaultOptions, $options);
        $options[':name'] .= ' '.$this->randomString();
        $this->query(
            'INSERT INTO "app_📚"("""title"" ?","👤", "🗓", """..., FROM ""` CREATE TABLE ;)  -? pÜre N😐nsense", '.
            '"ex ़ॏॠ༛ᇻቡᙻ⏏⏰⑰漣年", "↺") '.
            'VALUES ('.implode(', ', array_keys($options)).')',
            $options
        );
        $options[':id'] = $this->conn->lastInsertId();
        $this->deferQuery('DELETE FROM app_📚 WHERE "Numéro d\'identification" = ?', [$options[':id']]);
        return $options;
    }


    /**
     * Return a 8-character random printable string
     *
     * @return string
     */
    public function randomString()
    {
        return base64_encode(random_bytes(6));
    }

    /**
     * Execute deferred queries to clean up data
     */
    public function __destruct()
    {
        foreach ($this->deferredQueries as $query) {
            $this->query($query[0], $query[1]);
        }
    }
}
