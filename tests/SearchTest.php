<?php
namespace Tests;

use Facebook\WebDriver\WebDriverBy as By;
use Facebook\WebDriver\WebDriverSelect as Select;

/**
 * SearchTest : testing search in datagrids
 */
class SearchTest extends AtkTestCase
{
    /**
     * Simplest case : Searching by text on admin page
     */
    public function testSearchTextAdmin()
    {
        $employee = $this->db->addEmployee();
        $this->login();

        $this->goToEmployee('admin');
        $this->wd->findElement(By::id('App_employee_name'))->clear()->sendKeys($employee[':name']);
        $this->wd->findElement(By::cssSelector('input[value="Search"]'))->click();

        $this->assertEquals([$employee[':name']], array_keys($this->parseDataGrid()));
    }

    /**
     * Searching by substring (the default)
     */
    public function testSearchTextSubstring()
    {
        $employee = $this->db->addEmployee();
        $search = substr($employee[':name'], 2);
        $this->login();

        $this->goToEmployee('search');
        $this->wd->findElement(By::id('App_employee_name'))->clear()->sendKeys($search);
        (new Select($this->wd->findElement(By::name('atksearchmode_AE_name'))))->selectByValue('substring');
        $this->wd->findElement(By::cssSelector('button[type="submit"]'))->click();

        $this->assertEquals([$employee[':name']], array_keys($this->parseDataGrid()));
    }

    /**
     * Searching with exact match
     */
    public function testSearchTextExact()
    {
        $employee = $this->db->addEmployee();
        $this->login();

        $this->goToEmployee('search');
        $this->wd->findElement(By::id('App_employee_name'))->clear()->sendKeys($employee[':name']);
        (new Select($this->wd->findElement(By::name('atksearchmode_AE_name'))))->selectByValue('exact');
        $this->wd->findElement(By::cssSelector('button[type="submit"]'))->click();
        $this->assertEquals([$employee[':name']], array_keys($this->parseDataGrid()));

        $this->goToEmployee('search');
        $this->wd->findElement(By::id('App_employee_name'))->clear()->sendKeys(substr($employee[':name'], 2));
        (new Select($this->wd->findElement(By::name('atksearchmode_AE_name'))))->selectByValue('exact');
        $this->wd->findElement(By::cssSelector('button[type="submit"]'))->click();
        $this->assertCount(0, $this->parseDataGrid());
    }

    /**
     * Searching with wildcard
     */
    public function testSearchTextWildcard()
    {
        $employee = $this->db->addEmployee();
        $search = '*'.substr($employee[':name'], 2);
        $this->login();

        $this->goToEmployee('search');
        $this->wd->findElement(By::id('App_employee_name'))->clear()->sendKeys($search);
        (new Select($this->wd->findElement(By::name('atksearchmode_AE_name'))))->selectByValue('wildcard');
        $this->wd->findElement(By::cssSelector('button[type="submit"]'))->click();

        $this->assertEquals([$employee[':name']], array_keys($this->parseDataGrid()));
    }

    /**
     * Searching with regular expression
     */
    public function testSearchTextRegexp()
    {
        $employee1 = $this->db->addEmployee([':name' => 'Rose']);
        $employee2 = $this->db->addEmployee([':name' => 'Mary']);
        $this->login();

        $this->goToEmployee('search');
        $this->wd->findElement(By::id('App_employee_name'))->clear()->sendKeys('(Rose|Mary)');
        (new Select($this->wd->findElement(By::name('atksearchmode_AE_name'))))->selectByValue('regexp');
        $this->wd->findElement(By::cssSelector('button[type="submit"]'))->click();

        $names = array_keys($this->parseDataGrid());
        $this->assertCount(2, $names);
        $this->assertContains($employee1[':name'], $names);
        $this->assertContains($employee2[':name'], $names);
    }

    /**
     * Searching by ManyToOne relation target value
     */
    public function testSearchM2O()
    {
        $department = $this->db->addDepartment();
        $employee = $this->db->addEmployee([':department' => $department[':id']]);
        $this->login();

        $this->goToEmployee('admin', '', 'App.employeeRelation');
        $this->select2('atksearch_AE_department_AE_department[]', $department[':name']);
        $this->wd->findElement(By::cssSelector('input[value="Search"]'))->click();

        $this->assertEquals([$employee[':name']], array_keys($this->parseDataGrid()));
    }

    /**
     * Searching by ManyToOne relation with a select when target has a null field in descriptor.
     */
    public function testSearchM2OSelectDescriptorAndNullField()
    {
        $manager = $this->db->addEmployee([':email' => null]);
        $employee = $this->db->addEmployee([':manager' => $manager[':id']]);
        $this->login();

        $this->goToEmployee('admin', '', 'App.employeeRelation');
        $this->select2Keyboard('App_employeeRelation_manager_AE_manager', $manager[':name']);
        $this->wd->findElement(By::cssSelector('input[value="Search"]'))->click();

        $this->assertEquals([$employee[':name']], array_keys($this->parseDataGrid()));
    }

    /**
     * Searching by relation target when descriptor of target also includes relations
     */
    public function testSearchRelationDescriptorWithRelation()
    {
        $address = $this->db->addAddress([':zipcode' => $this->db->randomString(), ':city' => 'New Orleans']);
        $department = $this->db->addDepartment([':name' => 'Security']);
        $employee = $this->db->addEmployee([':department' => $department[':id'], ':address' => $address[':id']]);
        $this->login();

        $this->goToDepartment('admin');
        $this->wd->findElement(By::id('App_department_addresses'))->clear()->sendKeys($address[':zipcode']);
        $this->wd->findElement(By::cssSelector('input[value="Search"]'))->click();

        $this->assertEquals([$department[':name']], array_keys($this->parseDataGrid()));
    }

    /**
     * Searching by ManyToOne attribute added to grid with addListColumn
     */
    public function testSearchM2OListColumn()
    {
        $department1 = $this->db->addDepartment([':statuses' => 'need_to_change_boss']);
        $employee1 = $this->db->addEmployee([':department' => $department1[':id']]);
        $department2 = $this->db->addDepartment([':statuses' => '']);
        $employee2 = $this->db->addEmployee([':department' => $department2[':id']]);
        $this->login();

        $this->goToEmployee('admin', '', 'App.employeeRelation');
        $this->select2('atksearch_AE_department_AE_statuses[]', 'Need to change boss');
        $this->wd->findElement(By::cssSelector('input[value="Search"]'))->click();

        $found = array_keys($this->parseDataGrid());
        $this->assertContains($employee1[':name'], $found);
        $this->assertNotContains($employee2[':name'], $found);
    }

    /**
     * Searching by OneToMany targets descriptor content
     */
    public function testSearchO2M()
    {
        $department = $this->db->addDepartment();
        $employee = $this->db->addEmployee([
            ':department' => $department[':id'],
            ':email' => Db::randomString().'@co.co'
        ]);
        $searchStr = substr($employee[':name'].' ('.$employee[':email'].')', 4, 15);
        $this->login();

        $this->goToDepartment('admin');
        $this->wd->findElement(By::name('atksearch_AE_employees'))->clear()->sendKeys($searchStr);
        $this->wd->findElement(By::cssSelector('input[value="Search"]'))->click();

        $this->assertEquals([$department[':name']], array_keys($this->parseDataGrid()));
    }

    /**
     * Searching by OneToOne targets descriptor content
     */
    public function testSearchO2O()
    {
        $city = 'New '.Db::randomString();
        $address = $this->db->addAddress([':city' => $city]);
        $employee = $this->db->addEmployee([':address' => $address[':id']]);

        $this->login();

        $this->goToEmployee('admin', '', 'App.employeeRelation');
        $this->wd->findElement(By::name('atksearch_AE_address'))->clear()->sendKeys($city);
        $this->wd->findElement(By::cssSelector('input[value="Search"]'))->click();

        $this->assertEquals([$employee[':name']], array_keys($this->parseDataGrid()));
    }

    /**
     * Searching by Boolean value
     */
    public function testSearchBool()
    {
        $employee1 = $this->db->addEmployee([':name' => 'Jena', ':vacation' => true]);
        $employee2 = $this->db->addEmployee([':name' => 'Yoanis', ':vacation' => false]);
        $this->login();

        $this->goToEmployee('admin');
        (new Select($this->wd->findElement(By::name('atksearch_AE_vacation'))))->selectByValue(1);
        $this->wd->findElement(By::cssSelector('input[value="Search"]'))->click();
        $this->assertContains($employee1[':name'], array_keys($this->parseDataGrid()));

        $this->goToEmployee('admin');
        (new Select($this->wd->findElement(By::name('atksearch_AE_vacation'))))->selectByValue(0);
        $this->wd->findElement(By::cssSelector('input[value="Search"]'))->click();
        $this->assertContains($employee2[':name'], array_keys($this->parseDataGrid()));
    }

    /**
     * Search by date on admin page
     */
    public function testSearchDateAdmin()
    {
        $employee1 = $this->db->addEmployee([':hiredate' => '2004-02-09 14:00:00']);
        $employee2 = $this->db->addEmployee([':hiredate' => '2004-09-09 15:12:00']);
        $employee3 = $this->db->addEmployee([':hiredate' => '2005-08-25 08:00:00']);
        $employee4 = $this->db->addEmployee([':hiredate' => date('Y-05-30 09:05:00')]);
        $employee5 = $this->db->addEmployee([':hiredate' => date('Y-10-24 18:00:00')]);
        $this->login();

        // Entering a single year :
        $this->goToEmployee('admin');
        $this->wd->findElement(By::name('atksearch_AE_hiredate'))->clear()->sendKeys('2004');
        $this->wd->findElement(By::cssSelector('input[value="Search"]'))->click();
        $found = array_keys($this->parseDataGrid());
        $this->assertContains($employee1[':name'], $found);
        $this->assertContains($employee2[':name'], $found);
        $this->assertNotContains($employee3[':name'], $found);
        $this->assertNotContains($employee4[':name'], $found);
        $this->assertNotContains($employee5[':name'], $found);

        // Entering a incomplete date interval :
        $this->wd->findElement(By::name('atksearch_AE_hiredate'))->clear()
            ->sendKeys('05/2004-2006');
        $this->wd->findElement(By::cssSelector('input[value="Search"]'))->click();
        $found = array_keys($this->parseDataGrid());
        $this->assertNotContains($employee1[':name'], $found);
        $this->assertContains($employee2[':name'], $found);
        $this->assertContains($employee3[':name'], $found);
        $this->assertNotContains($employee4[':name'], $found);
        $this->assertNotContains($employee5[':name'], $found);

        // Entering date without year
        $this->wd->findElement(By::name('atksearch_AE_hiredate'))->clear()->sendKeys('10/5-15/6');
        $this->wd->findElement(By::cssSelector('input[value="Search"]'))->click();
        $found = array_keys($this->parseDataGrid());
        $this->assertNotContains($employee1[':name'], $found);
        $this->assertNotContains($employee2[':name'], $found);
        $this->assertNotContains($employee3[':name'], $found);
        $this->assertContains($employee4[':name'], $found);
        $this->assertNotContains($employee5[':name'], $found);
    }

    /**
     * Searching by aggregated column value
     */
    public function testSearchAggregatedColumn()
    {
        $randomStr = Db::randomString();
        $employee = $this->db->addEmployee([':color' => '#a82db9', ':notes' => $randomStr . ' but quiet.']);
        $this->login();

        $this->goToEmployee('admin');
        $this->wd->findElement(By::id('App_employee_characteristics'))->clear()->sendKeys('db9 - '.$randomStr);
        $this->wd->findElement(By::cssSelector('input[value="Search"]'))->click();
        $this->assertContains($employee[':name'], array_keys($this->parseDataGrid()));
    }

    /**
     * Search with multiple values for ListAttribute
     */
    public function testSearchListMultipleValue()
    {
        $department1 = $this->db->addDepartment([':statuses' => 'is_hiring']);
        $department2 = $this->db->addDepartment([':statuses' => 'will_be_closed_soon']);
        $department3 = $this->db->addDepartment([':statuses' => null]);
        $this->login();

        $this->goToDepartment('admin');
        $this->select2Keyboard('App_department_statuses', ['Is hiring', 'None']);

        $this->wd->findElement(By::cssSelector('input[value="Search"]'))->click();
        $found = array_keys($this->parseDataGrid());
        $this->assertContains($department1[':name'], $found);
        $this->assertNotContains($department2[':name'], $found);
        $this->assertContains($department3[':name'], $found);
    }

    /**
     * Search with multiple values for M2O Relation
     */
    public function testSearchM2OMultipleValues()
    {
        $department1 = $this->db->addDepartment();
        $department2 = $this->db->addDepartment();
        $employee1 = $this->db->addEmployee([':department' => $department1[':id']]);
        $employee2 = $this->db->addEmployee([':department' => $department2[':id']]);
        $employee3 = $this->db->addEmployee([]);
        $this->login();

        $this->goToEmployee('admin', 0, 'App.employeeRelation');
        $this->select2Keyboard('App_employeeRelation_department_AE_department', [$department1[':name'], $department2[':name']]);

        $this->wd->findElement(By::cssSelector('input[value="Search"]'))->click();
        $found = array_keys($this->parseDataGrid());
        $this->assertContains($employee1[':name'], $found);
        $this->assertContains($employee2[':name'], $found);
        $this->assertNotContains($employee3[':name'], $found);
    }

    /**
     * Search with multiple values for M2O Relation
     */
    public function testSearchM2OMultipleValuesWithNone()
    {
        $department1 = $this->db->addDepartment();
        $department2 = $this->db->addDepartment();
        $employee1 = $this->db->addEmployee([':department' => $department1[':id']]);
        $employee2 = $this->db->addEmployee([':department' => $department2[':id']]);
        $employee3 = $this->db->addEmployee([]);
        $this->login();

        $this->goToEmployee('admin', 0, 'App.employeeRelation');
        $this->select2Keyboard('App_employeeRelation_department_AE_department', [$department1[':name'], 'None']);

        $this->wd->findElement(By::cssSelector('input[value="Search"]'))->click();
        $found = array_keys($this->parseDataGrid());
        $this->assertContains($employee1[':name'], $found);
        $this->assertNotContains($employee2[':name'], $found);
        $this->assertContains($employee3[':name'], $found);
    }

    /**
     * Search M2O Relation with AF_LARGE flag
     */
    public function testSearchM2OLarge()
    {
        $manager = $this->db->addEmployee([':name' => 'Rachel']);
        $department1 = $this->db->addDepartment([':manager' => $manager[':id']]);
        $department2 = $this->db->addDepartment();
        $this->login();

        $this->goToDepartment('admin');
        $this->select2Keyboard('App_department_manager_AE_manager', $manager[':name']);

        $this->wd->findElement(By::cssSelector('input[value="Search"]'))->click();
        $found = array_keys($this->parseDataGrid());
        $this->assertContains($department1[':name'], $found);
        $this->assertNotContains($department2[':name'], $found);
    }

    /**
     * Saving search criteria
     */
    public function testSavedSearchCreateAndUse()
    {
        $name = 'my search'.Db::randomString();
        $employee1 = $this->db->addEmployee([':name' => 'Yannis', ':age' => 41]);
        $employee2 = $this->db->addEmployee([':name' => 'Marianne', ':age' => 41]);
        $employee3 = $this->db->addEmployee([':name' => 'Yannia', ':age' => 44]);

        $this->login();

        // Creating the search :
        $this->goToEmployee('search');
        $this->wd->findElement(By::id('App_employee_name'))->clear()->sendKeys('ann');
        $this->wd->findElement(By::name('atksearch_AE_age[from]'))->clear()->sendKeys('41');
        $this->wd->findElement(By::id('toggle_save_criteria'))->click();
        $this->wd->findElement(By::id('save_criteria'))->clear()->sendKeys($name);
        $this->wd->findElement(By::cssSelector('button[type="submit"]'))->click();
        $this->db->deferQuery('DELETE FROM atk_searchcriteria WHERE name = ?', [$name]);

        // Using the search :
        $this->goToEmployee('search');
        (new Select($this->wd->findElement(By::name('load_criteria'))))->selectByValue($name);
        $this->wd->findElement(By::cssSelector('button[type="submit"]'))->click();

        $found = array_keys($this->parseDataGrid());
        $this->assertContains($employee1[':name'], $found);
        $this->assertContains($employee2[':name'], $found);
        $this->assertNotContains($employee3[':name'], $found);
    }

    /**
     * Deleting search criteria
     */
    public function testSavedSearchDelete()
    {
        $name = 'other search'.Db::randomString();

        $this->login();

        // Creating the search :
        $this->goToEmployee('search');
        $this->wd->findElement(By::id('toggle_save_criteria'))->click();
        $this->wd->findElement(By::id('save_criteria'))->clear()->sendKeys($name);
        $this->wd->findElement(By::cssSelector('button[type="submit"]'))->click();
        $this->db->deferQuery('DELETE FROM atk_searchcriteria WHERE name = ?', [$name]);

        // Deleting the search :
        $this->goToEmployee('search');
        (new Select($this->wd->findElement(By::name('load_criteria'))))->selectByValue($name);
        $this->wd->findElement(By::cssSelector('a[title="Remove selected search"]'))->click();

        $this->goToEmployee('search');
        $savedSearches = $this->wd->findElement(By::name('load_criteria'))->getText();

        $this->assertNotContains($name, $savedSearches);
    }

    /**
     * Test that the 'total' line updates according to match values filtered by the search
     */
    public function testSearchTotal()
    {
        $employee1 = $this->db->addEmployee([':name' => 'Edwige', ':salary' => 2520]);
        $employee2 = $this->db->addEmployee([':name' => 'Robert', ':salary' => 1812]);

        $this->login();

        // Test initial total (should be at least 2520 + 1812) :
        $this->goToEmployee('admin');
        $total = $this->wd->findElements(By::className('recordList-totals-row'))[0]->getText();
        $this->assertGreaterThanOrEqual(2520+1812, (int) str_replace(',', '', substr(trim($total), 4, -3)));

        // Test search specific value
        $this->wd->findElement(By::id('App_employee_name'))->clear()->sendKeys($employee2[':name']);
        $this->wd->findElement(By::cssSelector('input[value="Search"]'))->click();
        $total = $this->wd->findElements(By::className('recordList-totals-row'))[0]->getText();
        $this->assertEquals(1812, (int) str_replace(',', '', substr(trim($total), 4, -3)));
    }
}
