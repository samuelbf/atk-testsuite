<?php
namespace Tests;

use Facebook\WebDriver\WebDriverBy as By;

/**
 * LoginTest : testing login page
 */
class LoginTest extends AtkTestCase
{
    /**
     * Testing default credentials
     */
    public function testAdministratorLogin()
    {
        $this->login('administrator', 'administrator');
        
        $username = $this->wd->findElement(By::cssSelector('p.navbar-text'));
        $this->assertEquals('administrator', $username->getText());
    }
    
    /**
     * Testing a random user login
     */
    public function testUserLogin()
    {
        $user = $this->db->addUser();
        $this->login($user[':username'], Db::DEFAULT_PASSWORD);
        
        $username_found = $this->wd->findElements(By::cssSelector('p.navbar-text'));
        $this->assertCount(1, $username_found);
        $this->assertEquals($user[':username'], $username_found[0]->getText());
    }

    /**
     * Testing rememberMe login
     */
    public function testRememberMeLogin()
    {
        $user = $this->db->addUser();
        $token = 'not-random-string';
        $selector = $this->db->randomString();
        $this->db->query(
            'INSERT INTO auth_rememberme(selector, token, username, expires, created) VALUES '.
            '(:selector, :token, :username, :expires, :created)',
            [':selector' => $selector,
                ':token' => hash('sha256', $token),
                ':username' => $user[':username'],
                ':created' => date('Y-m-d H:i:s'),
                ':expires' => '2035-01-01 00:00:00',
            ]
        );

        $this->wd->get($this->appAddress);
        $this->wd->manage()->addCookie(['name' => 'atk-testsuite-rememberme',
             'value' => $selector.':'.base64_encode($token),
             'secure' => false,
             'expiry' => time() + 10*24*3600,
             'httpOnly' => false,
             'path' => '/',
        ]);
        $this->wd->get($this->appAddress);

        $username_found = $this->wd->findElements(By::cssSelector('p.navbar-text'));
        $this->assertCount(1, $username_found);
        $this->assertEquals($user[':username'], $username_found[0]->getText());

        // Removing token renewed and stored in database by ATK :
        $this->db->query('DELETE FROM auth_rememberme where username = ?', [$user[':username']]);
    }
}
