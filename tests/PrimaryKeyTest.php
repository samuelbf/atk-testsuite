<?php
namespace Tests;

use Facebook\WebDriver\WebDriverBy as By;

/**
 * PrimaryKeyTest : testing behaviour for a complex primary key
 *
 * Here : Project Node, where primary key is a couple (name, startdate).
 */
class PrimaryKeyTest extends AtkTestCase
{
    /**
     * Check that the same key can't be used twice
     */
    public function testPrimaryKeyUnique()
    {
        $project = $this->db->addProject();
        $this->login();
        
        $this->goToProject('add');
        $this->wd->findElement(By::name('name'))->clear()->sendKeys($project[':name']);
        $this->wd->findElement(By::name('name'))->clear()->sendKeys($project[':name']);
        $this->setDate('startdate', explode('-', $project[':startdate']));
        $this->wd->findElement(By::cssSelector('button[type="submit"]'))->click();

        $this->assertTrue($this->hasErrorOnPage());
    }
    
    /**
     * Check that each member of the key can have duplicate (as long as the other member changes)
     */
    public function testPrimaryKeyPartsNotUnique()
    {
        $project = $this->db->addProject([':startdate' => '2019-01-01']);
        $this->login();

        // Test same name but different date
        $this->goToProject('add');
        $this->wd->findElement(By::name('name'))->clear()->sendKeys($project[':name']);
        $this->setDate('startdate', ['2019', '01', '02']);
        $this->wd->findElement(By::cssSelector('button[type="submit"]'))->click();
        $this->db->deferQuery(
            'DELETE FROM "app_project" WHERE "name" = :name AND "startdate" = :startdate',
            [':name' => $project[':name'], ':startdate' => '2019-01-02']
        );

        $this->assertFalse($this->hasErrorOnPage());

        // Test same date but different name
        $this->goToProject('add');
        $projectName = 'Toaster '.$this->db->randomString();
        $this->wd->findElement(By::name('name'))->clear()->sendKeys($projectName);
        $this->setDate('startdate', explode('-', $project[':startdate']));
        $this->wd->findElement(By::cssSelector('button[type="submit"]'))->click();
        $this->db->deferQuery(
            'DELETE FROM "app_project" WHERE "name" = :name AND "startdate" = :startdate',
            [':name' => $projectName, ':startdate' => '2019-01-01']
        );

        $this->assertFalse($this->hasErrorOnPage());
    }

    /**
     * Check view/edit buttons for a node with complex primary keys
     *
     * @param string $action View|Edit
     */
    private function testDGButton($action)
    {
        $project = $this->db->addProject();
        $this->login();

        $this->goToProject('admin');
        $this->parseDataGrid()[$project[':name']]['Actions'][$action]->click();

        $this->assertFalse($this->hasErrorOnPage());
        $title = $this->wd->getTitle();
        $this->assertContains($project[':name'], $title);
        $this->assertContains($action, $title);
    }

    /**
     * Check view button on admin page
     */
    public function testAdminView()
    {
        $this->testDGButton('View');
    }

    /**
     * Check edit button on admin page
     */
    public function testAdminEdit()
    {
        $this->testDGButton('Edit');
    }

    /**
     * Check delete button for a node with complex primary keys
     */
    public function testAdminDelete()
    {
        $project = $this->db->addProject();
        $this->login();

        $this->goToProject('admin');
        $dg = $this->parseDataGrid();
        $this->assertContains($project[':name'], array_keys($dg));
        $dg[$project[':name']]['Actions']['Delete']->click();

        $this->assertFalse($this->hasErrorOnPage());
        $this->wd->findElement(By::cssSelector('input[value="Yes"]'))->click();
        $this->assertFalse($this->hasErrorOnPage());
        $this->assertNotContains($project[':name'], array_keys($this->parseDataGrid()));
    }

    /**
     * Check that we can edit a project key and values in the same time
     */
    public function testPrimaryKeyEditKey()
    {
        $project = $this->db->addProject([':name' => 'Cars', ':startdate' => '2019-01-01']);
        $newName = 'Trucks '.$this->db->randomString();
        $description = 'We\'ll try bigger!';
        $this->login();

        $this->goToProject('edit', $project[':name'], $project[':startdate']);
        $this->wd->findElement(By::name('name'))->clear()->sendKeys($newName);
        $this->wd->findElement(By::name('description'))->clear()->sendKeys($description);
        $this->wd->findElement(By::cssSelector('button[type="submit"]'))->click();
        $this->db->deferQuery(
            'DELETE FROM "app_project" WHERE "name" = :name AND "startdate" = :startdate',
            [':name' => $newName, ':startdate' => $project[':startdate']]
        );

        // Check final values :
        $this->goToProject('view', $newName, $project[':startdate']);
        $displayedValues = $this->parseNodePage();
        $this->assertEquals($newName, $displayedValues['Name']->getText());
        $this->assertContains($description, $displayedValues['Description']->getText());
    }
}
