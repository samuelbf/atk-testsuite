#!/bin/sh
# Sample shell script to run the tests
# all arguments are passed to ./vendor/bin/steward.

# Setup environment :
. "./.env"
if [ "$DB_DRIVER" = "mysql" ]; then
    sudo systemctl start mysql
else
    sudo systemctl start postgresql
fi
cd ./vendor/bin/
java -jar selenium-server-standalone-*.jar > ../../logs/selenium.log 2>&1 &
PID_SELENIUM=$!
cd ../../
php -S "$APP_SERVER" -t web/ > logs/webserver.log 2>&1 &
PID_PHP=$!
sleep 3 # to give selenium the time to start

# Run the tests :
./vendor/bin/steward $*

# Close webserver & selenium :
kill -9 "$PID_PHP"
kill -9 "$PID_SELENIUM"
