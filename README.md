# ATK TestSuite

## What is this ?

The [ATK php framework](https://github.com/sintattica/atk/) allows you to write database-oriented PHP applications with very few lines of code.

This project aims to provide some functional tests in order to ease development of framework components. Running tests before committing changes should ensure that you d'ont break some functionnalities.

To automate the tests, this project relies on [Steward](https://github.com/lmc-eu/steward) (which itself is built upon [PHPUnit](https://phpunit.de/) and [Facebook PHP WebDriver](https://github.com/facebook/php-webdriver/)) and on [selenium](https://www.seleniumhq.org/) to manage the brower.

## Current status

- ATK php framework SHOULD NOT be used in production (security flaws).
- This test cover only a limited set of features. Our first goal is to test all features implemented in [ATK testbed](https://github.com/Sintattica/atk-testbed/).


## Copyright

(c) Samuel BF 2019
This is free software, release under Gnu General Public License version 3.0. See [LICENSE.txt](LICENSE.txt) for more details.

# Using the test suite

## Installation

For this to run, you will need MySQL, PHP and some PHP extensions as well, git and maybe some other programs.

Run or look into [setup.sh](setup.sh) to install necessary things. Necessary steps include :

1. Setting up .env file (see [.env.example](.env.example) to fill data)
1. Setting up the database and filling data from [mysql.setup.sql](mysql.setup.sql).
1. Installing dependencies with [composer](https://getcomposer.org)
1. Installing [selenium](https://www.seleniumhq.org/) and necessary webdriver (personnally, i work with [geckodriver](https://github.com/mozilla/geckodriver/releases)) into ./vendor/bin/

## Running the tests

The small [run.sh](run.sh) script ensures that MySQL, PHP and selenium servers are started before launching steward.

## Developping ATK

Start your developments under vendor/sintattica/atk :

- branch
- write code
- run tests
- commit

## Contributing to ATKTestSuite

All contributions are welcome. But this is very early development, so don't expect anything about stability of interfaces.
