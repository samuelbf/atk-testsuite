#!/bin/sh
# ATKtestsuite installation script
# (c) Samuel BF 2019
# 
# Provided AS IS without any warranty.
# See LICENSE.txt for information about license.


######################################
# Asking for configuration variables and putting them in .env
######################################
environmentVariable() {
	echo -n "$1 [$2] : "
	read answer
	if [ -z "${answer}" ]; then
		answer="$2"
	fi
	echo "export $3=\"${answer}\"" >> .env
}

echo 'export APP_ENV="dev"' > ./.env
environmentVariable "PHP server address" "localhost:8001" "APP_SERVER"
environmentVariable "MySQL host" "localhost" "MYSQL_HOST"
environmentVariable "MySQL database" "atktestsuite" "MYSQL_DATABASE"
environmentVariable "MySQL user" "atktestsuite" "MYSQL_USER"
environmentVariable "MySQL password (can be empty)" "" "MYSQL_PASSWORD"

######################################
# Creating/filling database
######################################
. ./.env
echo -n "Create MYSQL database and user automatically ? [y/n, def. y] : "
read answer
case "$answer" in
	[nN]* )
		echo -n "Please ensure the database is created then press enter"
		read;;
	* )
		user_host=""
		if [ "$MYSQL_HOST" = "localhost" ]; then
			user_host="@'localhost'"
		fi
		echo "DROP DATABASE IF EXISTS \`${MYSQL_DATABASE}\`;
DROP USER IF EXISTS '${MYSQL_USER}'${user_host};
CREATE DATABASE \`${MYSQL_DATABASE}\`;
CREATE USER '${MYSQL_USER}'${user_host} IDENTIFIED BY '${MYSQL_PASSWORD}';
GRANT ALL PRIVILEGES ON \`${MYSQL_DATABASE}\`.* TO '${MYSQL_USER}'${user_host};"  | sudo mysql;;
esac
mysql --user="${MYSQL_USER}" --password="${MYSQL_PASSWORD}"  "${MYSQL_DATABASE}" < mysql.setup.sql

######################################
# Fetching dependencies
######################################
if [ -x "$(command -v composer)" ]; then
	composer="composer"
else
	# Installing composer (from https://getcomposer.org/doc/faqs/how-to-install-composer-programmatically.md)
	EXPECTED_SIGNATURE="$(wget -q -O - https://composer.github.io/installer.sig)"
	php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
	ACTUAL_SIGNATURE="$(php -r "echo hash_file('sha384', 'composer-setup.php');")"

	if [ "$EXPECTED_SIGNATURE" != "$ACTUAL_SIGNATURE" ]
	then
		>&2 echo 'ERROR: Invalid installer signature'
		rm composer-setup.php
		exit 1
	fi

	php composer-setup.php --quiet
	RESULT=$?
	rm composer-setup.php
	if [ "$RESULT" -ne 0 ]; then
		>&2 echo 'ERROR: unable to install composer. See: https://getcomposer.org/.'
		exit 1
	fi
	composer="php composer.phar"
fi
$composer install
if [ "$?" -ne 0 ]; then
	>&2 echo 'ERROR: "${composer} install" failed.'
	exit 1
fi

./vendor/bin/steward install
if [ "$?" -ne 0 ]; then
	>&2 echo 'ERROR: "./vendor/bin/steward install" failed. Install manually from https://seleniumhq.org/'
	exit 1
fi

echo "Everything looks good. You'll probably want to run some tests with :"
echo ""
echo "		./run.sh run staging firefox [--pattern Some*Tests.php] [--filter testMySimpleTest]"
