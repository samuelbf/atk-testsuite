SET NAMES 'utf8';

-- ----------------------------
--  Cleaning up tables
-- ----------------------------
DROP TABLE IF EXISTS "atk_searchcriteria";
DROP TABLE IF EXISTS "atkeventlog";
DROP TABLE IF EXISTS "app_project_employee";
DROP TABLE IF EXISTS "app_employee_furniture";
DROP TABLE IF EXISTS "app_project";
DROP TABLE IF EXISTS "app_employee";
DROP TABLE IF EXISTS "app_department";
DROP TABLE IF EXISTS "app_address";
DROP TABLE IF EXISTS "app_furniture";
DROP TABLE IF EXISTS "app_📚";
DROP TABLE IF EXISTS "auth_users_groups";
DROP TABLE IF EXISTS "auth_u2f";
DROP TABLE IF EXISTS "auth_accessrights";
DROP TABLE IF EXISTS "auth_groups";
DROP TABLE IF EXISTS "auth_rememberme";
DROP TABLE IF EXISTS "auth_users";

-- ----------------------------
--  Table structure for authentication parts
-- ----------------------------
CREATE TABLE "auth_groups" (
  "id" SERIAL UNIQUE,
  "name" varchar(30) NOT NULL UNIQUE,
  "description" text,
  PRIMARY KEY ("id")
);

CREATE TABLE "auth_accessrights" (
  "node" varchar(150) NOT NULL,
  "action" varchar(100) NOT NULL,
  "group_id" integer NOT NULL REFERENCES "auth_groups"("id") ON UPDATE CASCADE ON DELETE CASCADE,
  PRIMARY KEY ("node", "action", "group_id")
);

CREATE TABLE "auth_rememberme" (
  "id" SERIAL UNIQUE,
  "selector" char(12) NOT NULL,
  "token" char(64) NOT NULL,
  "username" varchar(30) NOT NULL,
  "expires" timestamp NOT NULL,
  "created" timestamp NOT NULL,
  PRIMARY KEY ("id")
);

CREATE TABLE "auth_users" (
  "id" SERIAL UNIQUE,
  "username" varchar(30) NOT NULL UNIQUE,
  "passwd" varchar(255) NOT NULL,
  "firstname" varchar(255) NOT NULL DEFAULT '',
  "lastname" varchar(255) NOT NULL DEFAULT '',
  "email" varchar(255) NOT NULL DEFAULT '',
  "isDisabled" boolean NOT NULL DEFAULT '0',
  "isAdmin" boolean NOT NULL DEFAULT '0',
  "isU2FEnabled" boolean NOT NULL DEFAULT '0',
  PRIMARY KEY ("id")
);

CREATE TABLE "auth_u2f" (
  "id" SERIAL UNIQUE,
  "user_id" integer NOT NULL REFERENCES "auth_users"("id") ON UPDATE CASCADE ON DELETE CASCADE,
  "name" varchar(50) DEFAULT NULL,
  "keyHandle" varchar(255) NOT NULL,
  "publicKey" varchar(255) NOT NULL,
  "certificate" text NOT NULL,
  "counter" integer NOT NULL,
  PRIMARY KEY ("id")
);

CREATE TABLE "auth_users_groups" (
  "user_id" integer NOT NULL REFERENCES "auth_users"("id") ON UPDATE CASCADE ON DELETE CASCADE,
  "group_id" integer NOT NULL REFERENCES "auth_groups"("id") ON UPDATE CASCADE ON DELETE CASCADE,
  PRIMARY KEY ("user_id", "group_id")
);

CREATE TABLE "atkeventlog" (
  "id" SERIAL UNIQUE,
  "userid" integer NOT NULL,
  "stamp" timestamp NOT NULL,
  "node" varchar(100) NOT NULL,
  "action" varchar(100) NOT NULL,
  "primarykey" varchar(255),
  PRIMARY KEY ("id")
);

CREATE TABLE "atk_searchcriteria" (
  "name" varchar(100) NOT NULL,
  "nodetype" varchar(100) NOT NULL,
  "handlertype" varchar(100) NOT NULL,
  "criteria" varchar(4096),
  PRIMARY KEY ("name")
);

-- ----------------------------
--  Table structures for app_* tables
-- ----------------------------
CREATE TABLE "app_department"(
  "id" SERIAL UNIQUE,
  "name" varchar(100) NOT NULL,
  "statuses" varchar(255) DEFAULT NULL,
  "manager" int REFERENCES "app_employee"("id") ON UPDATE CASCADE ON DELETE SET NULL,
  PRIMARY KEY ("id")
);

CREATE TABLE "app_address"(
  "id" SERIAL UNIQUE,
  "address" varchar(300),
  "zipcode" varchar(8) NOT NULL,
  "city" varchar(100) NOT NULL,
  PRIMARY KEY ("id")
);

DROP TYPE IF EXISTS gender;
CREATE TYPE gender AS enum('M', 'F', 'O');
CREATE TABLE "app_employee"
(
  "id" SERIAL UNIQUE,
  "gender" gender DEFAULT NULL,
  "name" varchar(50) NOT NULL,
  "email" varchar(100) DEFAULT NULL,
  "age" int DEFAULT NULL,
  "color" varchar(7) DEFAULT NULL,
  "address" int DEFAULT NULL REFERENCES "app_address"("id") ON UPDATE CASCADE ON DELETE SET NULL,
  "department" int DEFAULT NULL REFERENCES "app_department"("id") ON UPDATE CASCADE ON DELETE SET NULL,
  "hiredate" timestamp DEFAULT NULL,
  "vacation" boolean DEFAULT FALSE,
  "notes" text,
  "blog" varchar(250),
  "qualities" int NOT NULL DEFAULT 0,
  "salary" decimal(10,2) DEFAULT NULL,
  "manager" int DEFAULT NULL REFERENCES "app_employee"("id") ON UPDATE CASCADE ON DELETE SET NULL,
  "createdby" int DEFAULT NULL REFERENCES auth_users(id) ON UPDATE CASCADE ON DELETE SET NULL,
  "createstamp" timestamp NOT NULL DEFAULT NOW(),
  "updatedby" int DEFAULT NULL REFERENCES auth_users(id) ON UPDATE CASCADE ON DELETE SET NULL,
  "updatestamp" timestamp NOT NULL DEFAULT NOW(),
  PRIMARY KEY ("id")
);

CREATE TABLE "app_project"(
  "name" varchar(25) NOT NULL,
  "startdate" date NOT NULL,
  "description" text,
  PRIMARY KEY ("name", "startdate")
);

CREATE TABLE "app_project_employee"(
  "name" varchar(25) NOT NULL,
  "startdate" date NOT NULL,
  "employee" integer NOT NULL REFERENCES "app_employee"("id") ON UPDATE CASCADE ON DELETE CASCADE,
  PRIMARY KEY ("name", "startdate", "employee"),
  FOREIGN KEY ("name", "startdate") REFERENCES "app_project"("name", "startdate") ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE "app_furniture"(
  "id" SERIAL UNIQUE,
  "name" varchar(50) NOT NULL,
  PRIMARY KEY ("id")
);

CREATE TABLE "app_employee_furniture"(
  "employee" int NOT NULL REFERENCES "app_employee"("id") ON UPDATE CASCADE ON DELETE CASCADE,
  "furniture" int NOT NULL REFERENCES "app_furniture"("id") ON UPDATE CASCADE ON DELETE CASCADE,
  PRIMARY KEY ("employee", "furniture")
);

-- To correctly read next lines, use a font with large coverage of utf8, such as symbola (http://users.teilar.gr/~g1951d/)
CREATE TABLE "app_📚"(
  "Numéro d'identification" SERIAL UNIQUE,
  """title"" ?" varchar(150),
  "👤" varchar(50),
  "🗓" timestamp,
  """..., FROM ""` CREATE TABLE ;)  -? pÜre N😐nsense" int,
  "ex ़ॏॠ༛ᇻቡᙻ⏏⏰⑰漣年" int,
  "↺" int REFERENCES "app_📚"("Numéro d'identification") ON UPDATE CASCADE ON DELETE SET NULL,
  PRIMARY KEY ("Numéro d'identification")
);

-- ----------------------------
--  Some data to fill in : sample user
-- ----------------------------
INSERT INTO "auth_users" ("id", "username", "passwd") VALUES
(1, 'sampleUser', '$2y$10$SwXvKhKCYmQ3eFKVcFr9.O5R2zBS2k4LfGmttrjU0dRoDwYnNUgCW');
-- password (unencrypted) : Qp+qsC^@=41
INSERT INTO "auth_groups" ("id", "name", "description") VALUES
(1, 'sampleGroup', 'Full rights to application table, not to the authentication ones');
INSERT INTO "auth_users_groups" ("user_id", "group_id") VALUES (1, 1);
SELECT nextval(pg_get_serial_sequence('auth_users', 'id'));
SELECT nextval(pg_get_serial_sequence('auth_groups', 'id'));
INSERT INTO "auth_accessrights" ("node", "action", "group_id") VALUES
('App.department', 'add', 1),
('App.department', 'admin', 1),
('App.department', 'export', 1),
('App.department', 'delete', 1),
('App.department', 'edit', 1),
('App.address', 'add', 1),
('App.address', 'admin', 1),
('App.address', 'delete', 1),
('App.address', 'edit', 1),
('App.addressSections', 'add', 1),
('App.addressSections', 'admin', 1),
('App.addressSections', 'delete', 1),
('App.addressSections', 'edit', 1),
('App.project', 'add', 1),
('App.project', 'admin', 1),
('App.project', 'export', 1),
('App.project', 'delete', 1),
('App.project', 'edit', 1),
('App.furniture', 'add', 1),
('App.furniture', 'admin', 1),
('App.furniture', 'export', 1),
('App.furniture', 'delete', 1),
('App.furniture', 'edit', 1),
('App.book', 'add', 1),
('App.book', 'admin', 1),
('App.book', 'export', 1),
('App.book', 'delete', 1),
('App.book', 'edit', 1),
('App.log', 'admin', 1),
('App.log', 'export', 1),
('App.employee', 'add', 1),
('App.employee', 'admin', 1),
('App.employee', 'export', 1),
('App.employee', 'delete', 1),
('App.employee', 'edit', 1),
('App.employee', 'raise_salary', 1),
('App.employeeSections', 'add', 1),
('App.employeeSections', 'admin', 1),
('App.employeeSections', 'export', 1),
('App.employeeSections', 'delete', 1),
('App.employeeSections', 'edit', 1),
('App.employeeTree', 'add', 1),
('App.employeeTree', 'admin', 1),
('App.employeeTree', 'export', 1),
('App.employeeTree', 'delete', 1),
('App.employeeTree', 'edit', 1),
('App.employeeRelation', 'add', 1),
('App.employeeRelation', 'admin', 1),
('App.employeeRelation', 'export', 1),
('App.employeeRelation', 'delete', 1),
('App.employeeRelation', 'edit', 1),
('App.employeeFilter', 'add', 1),
('App.employeeFilter', 'admin', 1),
('App.employeeFilter', 'delete', 1),
('App.employeeFilter', 'edit', 1);

